from flask import Flask, render_template, request, redirect
from jinja2.exceptions import TemplateNotFound
import os
import json

app = Flask(__name__)

os.environ['FLASK_APP'] = "main.py"
os.environ['FLASK_ENV'] = 'development'

buttons = json.load(open("./ssense_buttons.json"))

data_ = json.load(open("./shift_org_data.json"))

# @app.route("/SwitchHealth") #TODO: fix this
# def switch_health():
#     from hashids import Hashids
#     hashids = Hashids()
#     cur_hashid = url_path[14:]            
#     customer_phone_number = hashids.decode(cur_hashid)[0]
#     return

@app.route("/<string:org>")
def org_page(org: str):
    org = org.lower()
    if org not in data_:
        return render_template('index.html', org_name=org)
    else:
        d = data_[org]
        if 'url_redirect' in d:
            return redirect(d['url_redirect'])
        
        elif 'app_redirect' in d:
            if "iPhone" in request.headers["User-Agent"] or "iPad" in request.headers["User-Agent"]:
                return redirect(d['ios_redirect'])
            
            else:
                return redirect(d['android_redirect'])

        else:
            bg_color = 'black'
            if 'background_color' in d:
                bg_color = d['background_color']
            text_color = 'white'
            if 'text_color' in d:
                text_color = d['background_color']
            button_style = f"background-color: {bg_color};color: {text_color};"
            try:
                return render_template(
                    f'{org}.html',
                    page_title=org.capitalize(),
                    company_url=d['company_url'],
                    logo_file=f"{org}_logo.png",
                    button_style=button_style
                    )
            except TemplateNotFound:
                return render_template('index.html', org_name=org)


@app.route("/ssense/")
def ssense_demo(): # landing page
    last_page = request.form.get('source_page')
    if not last_page:
        last_page=""
    return render_template(
        'ssense_demo/template.html', 
        buttons=buttons['index'],
        last_page=last_page,
        source_page='other_countries')


@app.route("/ssense/<string:page_>/")
def ssense_path(page_: str):
    page_ = [p for p in page_.split("/") if p][-1]
    last_page = request.args.get('source_page')

    print(f"page: {page_}, last_page: {last_page}")
    print(request.referrer)

    if not last_page:
        last_page=""

    buttons_ = buttons['index']
    if page_ in buttons:
        buttons_ = buttons[page_]

    return render_template(
        'ssense_demo/template.html',
        buttons=buttons_,
        source_page=page_,
        last_page=last_page
        )


if __name__ == "__main__":
    app.run(debug=True)
