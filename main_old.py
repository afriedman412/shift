 # -*- coding: UTF-8 -*-
#!/usr/bin/env python
import webapp2
import logging
from google.appengine.ext import ndb

class ShortLink(ndb.Model):
    
    ip_address = ndb.StringProperty(indexed=False)
    url_path = ndb.StringProperty(indexed=True)
    click_date = ndb.DateTimeProperty(auto_now_add=True)


def isNowInTimePeriod(startTime, endTime, nowTime):

    if startTime < endTime:
        return nowTime >= startTime and nowTime <= endTime
    else: #Over midnight
        return nowTime >= startTime or nowTime <= endTime

def GetTime(CurTZ, WeekdayStart, WeekdayEnd, WeekendStart, WeekendEnd, WeekdayOnly):

    from datetime import datetime
    from pytz import timezone

    CompanyBusinessHours = True

    tz = timezone(CurTZ)
    nowPST = datetime.now(tz)
    nowTimePST = nowPST.time()

    if nowPST.isoweekday() in range(1, 6):

        # Monday to Friday

        startTime = datetime.strptime(WeekdayStart, '%H:%M').time()
        endTime = datetime.strptime(WeekdayEnd, '%H:%M').time()

        if isNowInTimePeriod(startTime, endTime,nowTimePST):

            # Monday to Friday // 6am-8pm, PST // Business Hours
            CompanyBusinessHours = True
            return CompanyBusinessHours            
        else:
            
            # Monday to Friday // Outside Business Hours
            CompanyBusinessHours = False
            return CompanyBusinessHours
    else:

        if(WeekdayOnly == True):

            CompanyBusinessHours = False
            return CompanyBusinessHours

        else:
            
            # Saturday & Sunday
            startTime = datetime.strptime(WeekendStart, '%H:%M').time()
            endTime = datetime.strptime(WeekendEnd, '%H:%M').time()

            if isNowInTimePeriod(startTime, endTime,nowTimePST):

                # Saturday & Sunday // 7:30am - 8pm, PST // Business Hours
                CompanyBusinessHours = True
                return CompanyBusinessHours
            
            else:

                # Saturday & Sunday // Outside Business
                CompanyBusinessHours = False
                return CompanyBusinessHours

    return BusinessHoursMessage    

def GetVegaHTML():

    CompanyBusinessHours = True
        
    CurTZ = "America/Los_Angeles"
    WeekdayStart = "07:00"
    WeekdayEnd = "17:00"
    WeekendStart = ""
    WeekendEnd = ""
    
    WeekdayOnly = True
    
    
    CompanyBusinessHours = GetTime(CurTZ, WeekdayStart, WeekdayEnd, WeekendStart, WeekendEnd, WeekdayOnly)
    
    if(CompanyBusinessHours == False):
        
        htmlPage = """
            
            <html>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <head>
            <title>Vega - Clean, Plant-based Nutrition</title>
            </head>
            <body style="background-color:#6C8F21;color:white">
            <center>
            <a href="http://www.myvega.com"><img src="vega_logo.png" style="max-width:150px"></a>
            
            <p style='font-size:16px'>Our chat support team is currently unavailable. <br>Please contact us during our regular hours.</p>
            
            <p style='font-size:16px'>Monday - Friday: 7am-5pm Pacific Time</p>
            
            <p style='font-size:16px'>Visit our <a style="color:white;font-weight:bold" href="http://www.myvega.com/blogs/faqs">Help Center</a><br>for FAQs and additional support options</p>
            </center>
            </body>
            </html>
            """
        
        return htmlPage
    
    else:
        
        htmlPage = """

            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html style="display:none !important;">
            <head><script src="https://myvega.secure.force.com/static/111213/js/perf/stub.js" type="text/javascript"></script>
            <script src="https://1489.la1-c1-ia2.salesforceliveagent.com/content/g/js/38.0/prechat.js" type="text/javascript"></script><script type="text/javascript">
            //<![CDATA[
            try{(function(){var w=self,l,p,o;if(w&&top){for(;w!==top&&(p=w.parent)&&p!==w&&(o=p.location)&&o.protocol===(l=w.location).protocol&&(o.host===l.host||(p.document&&p.document.domain===w.document.domain));w=p);if(w!==top)throw "";}({f:function(){document.documentElement?document.documentElement.style.display="":(!this.a&&(this.a=10),this.a<1E5&&(window.sfdcRetryShowWindow=this)&&setTimeout("sfdcRetryShowWindow.f()",this.a),this.a*=2)}}.f())})();}catch(e){if(top!==self)top.location=location;else throw e;}//]]></script></head><body>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" />
            <meta HTTP-EQUIV="Expires" content="Mon, 01 Jan 1990 12:00:00 GMT" />
            
            <title>Vega Chat</title>
            <style>
            body {
            font-family: sans-serif;
            color:white;
            }
            #pre-chat_container {
            }
            #pre-chat_content {
            background-color: #8AAB2A;
            padding-top:180px;
            }
            #pre-chat_content .row {
            display:flex;
            }
            #pre-chat_content .row .label {
            text-align:left;
            margin-left:180px;
            }
            #pre-chat_content .row  .value {
            margin-left:180px;
            }
            #pre-chat_content .row  .button {
            margin-top:10px;
            margin-left:180px;
            }
            #pre-chat_content .row .header-image {
            margin: auto;
            }
            
            #pre-chat_content .spacer-after-image {
            margin-top:15px;
            }
            </style>
            
            <meta content="ie=edge" http-equiv="x-ua-compatible" />
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
            <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport" />
            </head>
            <body style="background-color:#6C8F21;">
            
            <form action="https://1489.la1-c1-ia2.salesforceliveagent.com/content/s/chat?language=en_US#deployment_id=57236000000LFlA&amp;org_id=00D36000000rG4P&amp;button_id=57336000000LGnK" id="prechatForm" method="post">
            
            <center>
            
            <div style="padding-top:50px;color:white;height:100%">
            <a href="http://www.myvega.com"><img src="vega_logo.png" style="max-width:150px"></a>
            <div style="text-align:left;width:150px;margin-left:20px;padding-top:10px">
            <div style="padding-bottom:5px;font-size:19px">First Name <span style="color: red;">*</span> :</div>
            
            <input id="AccountFirstName" name="liveagent.prechat:AccountFirstName" required="true" type="text" /><br><br>
            <div style="padding-bottom:5px;font-size:19px">Last Name :</div>
            <input id="AccountLastName" name="liveagent.prechat:AccountLastName" type="text" />
            <br><br>
            <div style="padding-bottom:5px;font-size:19px">Email<span style="color: red;">*</span> :</div>
            <input id="AccountEmail" name="liveagent.prechat:AccountEmail" onchange="try{setCustomValidity('')}catch(e){}" oninvalid="setCustomValidity('Please enter a valid email address')" pattern="[^ @]*@[^ @]*\.[^ @]*" required="true" type="email" />
            
            <br><br>
            <div class="button" style="margin-top:10px">
            <input id="prechat_submit" type="submit" value="Chat Now" style="font-size:19px;width:135px;padding:10px;font-family:'Arial'" />
            </div>
            
            
            
            </div>
            </div>
            </center>
            
            
            <input name="liveagent.prechat:PreChatCurrencyIsoCode" type="hidden" value="CAD" />
            <input name="liveagent.prechat:AccountRecordType" type="hidden" value="012360000019z9OAAQ" />
            <input name="liveagent.prechat.findorcreate.map:Account" type="hidden" value="RecordTypeId,AccountRecordType;PersonEmail,AccountEmail;FirstName,AccountFirstName;LastName,AccountLastName;CurrencyIsoCode,PreChatCurrencyIsoCode" />
            <input name="liveagent.prechat.findorcreate.map.doFind:Account" type="hidden" value="PersonEmail,true" />
            <input name="liveagent.prechat.findorcreate.map.isExactMatch:Account" type="hidden" value="PersonEmail,true" />
            <input name="liveagent.prechat.findorcreate.map.doCreate:Account" type="hidden" value="RecordTypeId,true;FirstName,true;LastName,true;PersonEmail,true;CurrencyIsoCode,true" />
            <input name="liveagent.prechat.findorcreate.showOnCreate:Account" type="hidden" value="true" />
            
            
            <input name="liveagent.prechat:PreChatCaseSubject" type="hidden" value="Live Agent Chat" />
            <input name="liveagent.prechat:PreChatCaseStatus" type="hidden" value="New" />
            <input name="liveagent.prechat:PreChatCaseRecordType" type="hidden" value="012360000019z9nAAA" />
            <input name="liveagent.prechat.findorcreate.map:Case" type="hidden" value="Status,PreChatCaseStatus;RecordTypeId,PreChatCaseRecordType;Subject,PreChatCaseSubject" />
            <input name="liveagent.prechat.findorcreate.map.doCreate:Case" type="hidden" value="Status,true;Subject,true;RecordTypeId,true" />
            <input name="liveagent.prechat.findorcreate.showOnCreate:Case" type="hidden" value="true" />
            
            
            <input name="liveagent.prechat.findorcreate.saveToTranscript:Case" type="hidden" value="CaseId" />
            <input name="liveagent.prechat.findorcreate.saveToTranscript:Account" type="hidden" value="AccountId" />
            <input name="liveagent.prechat.findorcreate.linkToEntity:Account" type="hidden" value="Case,AccountId" />
            
            </form>
            <script type="text/javascript">
            // Initialising Pre-Chat
            liveagent.details.preChatInit('https://d.la4-c1-chi.salesforceliveagent.com/chat', 'detailCallback');
            
            /**
            * Custom Detail Pre-Chat init callback
            * @param  {Object} details     Object of pre-chat details
            */
            var detailCallback = function (details) {
            console.log('-- Setting form values from Chat button ---');
            
                        for (var i = 0; i < details.customDetails.length; i++) {
                          var detail = details.customDetails[i];
                        console.log(detail);
            
                      if (document.getElementById(detail.label)) {
                        document.getElementById(detail.label).value = detail.value;
                  }
            }
            };
            
            /**
            * Executed when Pre-Chat form is submitted
            */
            function onSubmit() {
            //var fullName = document.getElementById("ContactFirstName").value + " " + document.getElementById("ContactLastName").value;
            
            //document.getElementById("prechat_field_name").value = fullName;
            //document.getElementById("visitorName").value = fullName;
            }
            </script>
            </body></body></html>





        """

        return htmlPage


def GetPrettyLitterHTML():

    CompanyBusinessHours = True

    CurTZ = "America/Los_Angeles"
    WeekdayStart = "07:00"
    WeekdayEnd = "17:00"
    WeekendStart = ""
    WeekendEnd = ""

    WeekdayOnly = True
    

    CompanyBusinessHours = GetTime(CurTZ, WeekdayStart, WeekdayEnd, WeekendStart, WeekendEnd, WeekdayOnly)

    if(CompanyBusinessHours == False):

        htmlPage = """<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>Chat Pretty Litter - Cat Litter That Protects Your Cat</title>
         <link rel="stylesheet" type="text/css" media="all" href="https://cdn3.minted.com/assets/2e5f6905/static/compiled/css/v2/themes/default/site.css?v=y_jU9JHVRA" />
        </head>
    <body>
        <center>
        <a href="http://www.prettylitter.co"><img src='/prettylitter/prettylitter_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
    
            <p style='font-size:16px'>Our chat support team is currently unavailable. <br>Please contact us during our regular hours.</p><br><br>
    
            <p style='font-size:16px'>Monday - Friday: 7am-5pm US Pacific Time</p>            
            <br><br>
            <p style='font-size:16px'>Visit our <a style="color:#51909C" href="http://prettylitter.co/frequently-asked-questions/">Help Center</a><br>for FAQs and additional support options</p>
        </center>
    </body>
</html>
        """

        return htmlPage

    else:

        htmlPage = """

<!DOCTYPE html><meta name="viewport" content="width=device-width, initial-scale=1">
<html dir="ltr" lang="en-US">
    
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    
    function clickButton(){             
                 
                           $("div").trigger('click');

    }

                            
                            
                            
    </script>

    </head>
    
                <!-- Start of Kustomer widget -->
                
                <body>
                    
                    <script>

!function(a,b,c,d){a.Kustomer=c,c._q=[],c._i=[],c.init=function(a){function b(a,b){a[b]=function(){a._q.push([b].concat(Array.prototype.slice.call(arguments,0)))}}for(var d="init clear identify track start describe".split(" "),e=0;e<d.length;e++)b(c,d[e]);c._i.push(a)};var e=b.createElement("script");e.type="text/javascript",e.async=!0,e.src="https://cdn.kustomerapp.com/cw/sdk.v1.1.min.js";var f=b.getElementsByTagName("script")[0];f.parentNode.insertBefore(e,f)}(this,document,window.Kustomer||{});                        
                         
                        </script>
                    
                    <center>
                        
                        <p><a href="http://www.prettylitter.co"><img src='/prettylitter/prettylitter_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>

                        <span style="color:white;background-color:#6FC2D4;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="clickButton();"> chat with us </span>
                    </center>
                    
                </body>
                
                <script>
                    
Kustomer.init('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhNmZiYWNmNjgxNWY1MDAyMGIzNTc5YiIsInVzZXIiOiI1YTZmYmFjZjk0NGI2YTAwMDE2ZDI3YWMiLCJvcmciOiI1YTZmYmFjZjI3M2JkMDAwMDFhM2ZlOTQiLCJvcmdOYW1lIjoicHJldHR5LWxpdHRlciIsInVzZXJUeXBlIjoibWFjaGluZSIsInJvbGVzIjpbIm9yZy50cmFja2luZyJdLCJhdWQiOiJ1cm46Y29uc3VtZXIiLCJpc3MiOiJ1cm46YXBpIiwic3ViIjoiNWE2ZmJhY2Y5NDRiNmEwMDAxNmQyN2FjIn0.byyyOyEjSf7InBJZRwLO7WbEDs5-jzPtvy9XOwC6HJ4'); 
                    
                    Kustomer.start();
                    
                    </script>

 <style>
    .sb-avatar > div {
      background-color: white !important;
    }
    #content__add_product .product-item,#content__add_product #pagination,#content__add_product  #page_results{
      display: none !important;
    }
    #content__add_product .product-3928584068, #content__add_product .product-3928587908, #content__add_product .product-3928579012 {
      display: inline-block !important;
    }
    .kustomer-app-icon {
      width: 60px !important;
      height: 60px !important;
      background-color: transparent;
    }
    .kustomer-app-icon:before {
      content: none;
    }
    .kustomer-app-icon:after {
      content: none;
    }
    .kustomer-app-icon > div {
      height: 60px !important;
      width: 60px !important;
      border: 1px solid #6bc2d6 !important;
      border-radius: 50% !important;
      background-color: transparent !important;
      background-size: 75% !important;
    }
    
    .minimizedUnreadIcon[style] {
      top: 50% !important;
      left: 100% !important;
      transform: translate(-50%, -50%) !important;
      padding: 3px !important;
      background: red !important;
      height: 18px !important;
      width: 18px !important;
      border-radius: 50% !important;
      color: white !important;
      font-size: 12px !important;
    }
    
    @media screen and (min-width:767px) {
      .minimizedUnreadIcon[style] {
      top: -12px !important;
      left: -12px !important;
      transform: none !important;
      font-size: 14px !important;
      padding: 5px !important;
      height: 24px !important;
      width: 24px !important;
    }
      .customers-account .main.content > p:first-of-type  {
        display: inline-block;
        padding: 15px;
        background-color: #ffffff;
        position: fixed;
      }
      .kustomer-app-icon > div {
        display: none;
      }
      .kustomer-app-icon {
        width: auto !important;
        height: auto !important;
        padding: 4px 8px !important;
        bottom: 0 !important;
        right: 10px !important;
        display: inline-block !important;
        color: #fff !important;
        background-color: #6bc2d6 !important;
        border-radius: 4px 4px 0 0 !important;
      }
      .kustomer-app-icon:before {
        content: "\f0d8";
        font-family: 'Font Awesome\ 5 Free';
        font-weight: 900;
      }
      .kustomer-app-icon:after {
        content: ' chat with us'
      }
    }  
    @media (max-width: 600px) {
      .rmq-acfd08ab.enterIcon {
        display: block !important;
      }
    }
  </style>
                
                <!-- End of Kustomer widget -->





                
    </body>
    
</html>


        """

        return htmlPage


def GetMintedHTML(hashid):

    MintedBusinessHours = True

    CurTZ = "America/Los_Angeles"
    WeekdayStart = "06:00"
    WeekdayEnd = "20:00"
    WeekendStart = "07:30"
    WeekendEnd = "20:00"
    WeekdayOnly = False

    MintedBusinessHours = GetTime(CurTZ, WeekdayStart, WeekdayEnd, WeekendStart, WeekendEnd, WeekdayOnly)

    if(MintedBusinessHours == False):

        htmlPage = """<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>Chat Minted - Wedding Invitations, Save the Date Cards, Thank You cards</title>
         <link rel="stylesheet" type="text/css" media="all" href="https://cdn3.minted.com/assets/2e5f6905/static/compiled/css/v2/themes/default/site.css?v=y_jU9JHVRA" />
        </head>
    <body>
        <center>
        <a href="http://www.minted.com"><img src='http://hlp.chat/minted/minted_logo_phone.png' style='margin-top:70px;margin-bottom:40px'></a>
    
            <p style='font-size:16px'>Our chat support team is currently unavailable. <br>Please contact us during our regular hours.</p><br><br>
    
            <p style='font-size:16px'>Monday - Friday: 6am-8pm US Pacific Time</p>
    
            <p style='font-size:16px'>Saturday - Sunday: 7:30am-8pm US Pacific Time</p>
            
            <br><br>
            <p style='font-size:16px'>Visit our <a href="https://www.minted.com/help">Help Center</a><br>for FAQs and additional support options</p>
        </center>
    </body>
</html>
        """

        return htmlPage

    else:

        htmlPage = """<html>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <head><title>Minted Chat - Wedding Invitations, Save the Date Cards, Thank You cards</title>
        <link rel="apple-touch-icon" sizes="57x57" href="/minted/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/minted/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/minted/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/minted/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/minted/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/minted/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/minted/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/minted/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/minted/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/minted/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/minted/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/minted/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/minted/favicon-16x16.png">
        <link rel="manifest" href="/minted/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/minted/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
             <link rel="stylesheet" type="text/css" media="all" href="https://cdn3.minted.com/assets/2e5f6905/static/compiled/css/v2/themes/default/site.css?v=y_jU9JHVRA" />
            <script type="text/javascript" src="../hashids.min.js"></script>
            <script type="text/javascript" src="https://c.la1-c1-iad.salesforceliveagent.com/content/g/js/29.0/deployment.js"></script>
            </head>
        <body>
            <center><a  onclick="liveagent.startChat('573f30000008PQz');">
            <img src='/minted/minted_logo_phone.png' style='margin-top:100px;margin-bottom:40px'></a>
            <p><a id="liveagent_button_online_573f30000008PQz"  onclick="liveagent.startChat('573f30000008PQz');" class="chatCategory submitButton button buttonFlat buttonSecondary buttonMedium bm" style="font-size:20px;padding-bottom:20px">Start Chat
            </a></p>
            </center>
            <script type='text/javascript'>

                liveagent.showWhenOnline('573f30000008PQz', document.getElementById('liveagent_button_online_573f30000008PQz'));

                cur_id = "%s";
                var hashids = new Hashids();
                var caller_phone_number = hashids.decode(cur_id);
                caller_phone_number = caller_phone_number.toString();
                var cleanNum = caller_phone_number;
                if(cleanNum.length == 10){
                    cleanNum = cleanNum.substring(0,3) + "-" + cleanNum.substring(3,6) + "-" + cleanNum.substring(6);
                }

                var clean_name = cleanNum + " (Chatdesk)";
                // Sets the display name of the visitor in the agent console when engaged in a chat
                liveagent.setName(clean_name);
                                 
                // Creates a custom detail called Phone Number and sets the value 
                liveagent.addCustomDetail("Shipping Phone", caller_phone_number, true);
                liveagent.addCustomDetail("Billing Phone", caller_phone_number, true);

                liveagent.findOrCreate("Account").map("Shipping_Phone_Number__c","Shipping Phone",true,false,true).map("Billing_Phone_Number__c","Billing Phone",true,false,true).saveToTranscript("AccountId").showOnCreate().linkToEntity("Case","AccountId");
     
                liveagent.init('https://c.la1-c1-iad.salesforceliveagent.com/chat', '57240000000CbAm', '00D40000000Mx87');

            </script>
            </body>
        </html>
        """ % hashid

        return htmlPage

class urlRedirect(webapp2.RequestHandler):
  def get(self):

        url_path = self.request.path
        followRedirect = True        

        redirectURL = ""
        redirectType = "server"

        if(url_path == "/BlueApron"):
#            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4EVFt7f76ehzJTmILi9aOIQF6BpJaWD4"

            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.headers['Access-Control-Allow-Origin'] = '*.my.salesforce.com'
            self.response.headers['Access-Control-Allow-Origin'] = '*.force.com'
            self.response.headers['Access-Control-Allow-Origin'] = '*.salesforce.com'

            htmlPage = """
<html><body>
<style type='text/css'>
	.embeddedServiceHelpButton .helpButton .uiButton {
		background-color: #005290;
		font-family: "Salesforce Sans", sans-serif;
	}
	.embeddedServiceHelpButton .helpButton .uiButton:focus {
		outline: 1px solid #005290;
	}
	@font-face {
		font-family: 'Salesforce Sans';
		src: url('https://www.sfdcstatic.com/system/shared/common/assets/fonts/SalesforceSans/SalesforceSans-Regular.woff') format('woff'),
		url('https://www.sfdcstatic.com/system/shared/common/assets/fonts/SalesforceSans/SalesforceSans-Regular.ttf') format('truetype');
	}
</style>

<script type='text/javascript' src='https://service.force.com/embeddedservice/5.0/esw.min.js'></script>
<script type='text/javascript'>
	var initESW = function(gslbBaseURL) {
		embedded_svc.settings.displayHelpButton = true; //Or false
		embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'

		//embedded_svc.settings.defaultMinimizedText = '...'; //(Defaults to Chat with an Expert)
		//embedded_svc.settings.disabledMinimizedText = '...'; //(Defaults to Agent Offline)

		//embedded_svc.settings.loadingText = ''; //(Defaults to Loading)
		//embedded_svc.settings.storageDomain = 'yourdomain.com'; //(Sets the domain for your deployment so that visitors can navigate subdomains during a chat session)

		// Settings for Live Agent
		//embedded_svc.settings.directToButtonRouting = function(prechatFormData) {
			// Dynamically changes the button ID based on what the visitor enters in the pre-chat form.
			// Returns a valid button ID.
		//};
		//embedded_svc.settings.prepopulatedPrechatFields = {}; //Sets the auto-population of pre-chat form fields
		//embedded_svc.settings.fallbackRouting = []; //An array of button IDs, user IDs, or userId_buttonId
		//embedded_svc.settings.offlineSupportMinimizedText = '...'; //(Defaults to Contact Us)

                embedded_svc.settings.extraPrechatFormDetails = [{
                     "label":"CartValue",
                     "value":"200",
                     "transcriptFields":[ "CartValue__c" ],
                     "displayToAgent":true
                }];


		embedded_svc.settings.enabledFeatures = ['LiveAgent'];
		embedded_svc.settings.entryFeature = 'LiveAgent';

		embedded_svc.init(
			'https://chatdesk.my.salesforce.com',
			'https://chatdesk.force.com/support',
			gslbBaseURL,
			'00Df2000001G8p5',
			'ch',
			{
				baseLiveAgentContentURL: 'https://c.la4-c2-phx.salesforceliveagent.com/content',
				deploymentId: '572f2000000Tpfp',
				buttonId: '573f2000000TqC4',
				baseLiveAgentURL: 'https://d.la4-c2-phx.salesforceliveagent.com/chat',
				eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04If2000000PBcwEAG_1679d28cdf0',
				isOfflineSupportEnabled: false
			}
		);
	};

	if (!window.embedded_svc) {
		var s = document.createElement('script');
		s.setAttribute('src', 'https://chatdesk.my.salesforce.com/embeddedservice/5.0/esw.min.js');
		s.onload = function() {
			initESW(null);
		};
		document.body.appendChild(s);
	} else {
		initESW('https://service.force.com');
	}
</script>
</body>
</html>
            """
            self.response.write(htmlPage)

        elif(url_path == "/Abercrombie"):
            followRedirect = False

            # Prod server
            redirectURL = "https://abercrombie.custhelp.com/app/chat/chat_launch_no_persist/custbrand/af/_icf_17/70/incidents.c$language/70/_icf_39/116/incidents.c$site/116/_icf_84/228/incidents.c$brand/228/_icf_124/IVR/incidents.c$chat_type/IVR/_icf_127/service/incidents.c$line_of_business/service"
            
            htmlPage = """<html><head>
            <meta property="og:title" content="Abercrombie & Fitch Chat" />
            <meta property="og:image" content="/abercrombie/favicon-32x32.png" />
            <meta property="og:description" content="Abercrombie & Fitch Chat" />
            <meta property="og:url" content="http://www.hlp.chat/Abercrombie" />
            <meta property="og:site-name" content="Abercrombie & Fitch Chat"/>
            <meta property="og:type" content="website" />
            <Title>Abercrombie & Fitch Chat</Title>    
            <link rel="apple-touch-icon" sizes="57x57" href="/abercrombie/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="/abercrombie/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="/abercrombie/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="/abercrombie/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="/abercrombie/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="/abercrombie/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="/abercrombie/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="/abercrombie/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="/abercrombie/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="/abercrombie/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/abercrombie/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="/abercrombie/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/abercrombie/favicon-16x16.png">
            <link rel="manifest" href="/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/abercrombie/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <script>window.location = '%s';</script></head><body></body></html>""" % redirectURL

            self.response.write(htmlPage)


        elif(url_path == "/AF"):
            
            redirectURL = "https://www.abercrombie.com/shop/CustomerService?pageName=texts-terms&storeId=10051&textKey=LEGAL_TEXTS_TERMS&catalogId=10901&langId=-1"

        elif(url_path == "/AptDeco"):

            htmlPage = """

<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
<title>AptDeco Support</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>

                    <center>
                        <p><a href="http://www.aptdeco.com"><img src='/aptdeco/aptdeco_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#7E7E7E;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="FrontChat('show');"> Chat with us </span>
                        
                    </center>


<script src="https://chat-assets.frontapp.com/v1/chat.bundle.js"></script>
<script>
  window.FrontChat('init', {chatId: '4e363ab9a92a5ffa6d4e3f3ac9c8c4a1', useDefaultLauncher: true});        
</script>
<script>
    FrontChat('show');
                        $(document).ready(function(){
                            FrontChat('show');

                        });
 
                
                        </script>




    
</body>
</html>
            """
            
            self.response.write(htmlPage)

        elif(url_path == "/AptDecoCOI"):

            redirectURL = "https://aptdeco.typeform.com/to/v2CVjB"

        elif(url_path == "/Hollister"):
            followRedirect = False

            # Prod server
            redirectURL = "https://hollisterco.custhelp.com/app/chat/chat_launch/custbrand/hco/_icf_17/70/incidents.c$language/70/_icf_39/116/incidents.c$site/116/_icf_84/229/incidents.c$brand/229/_icf_124/IVR/incidents.c$chat_type/IVR/_icf_127/service/incidents.c$line_of_business/service"

#           Dev server
#            redirectURL = "https://hollisterco--tst.custhelp.com/app/chat/chat_launch/custbrand/hco/_icf_17/70/incidents.c$language/70/_icf_39/116/incidents.c$site/116/_icf_84/229/incidents.c$brand/229/_icf_124/IVR/incidents.c$chat_type/IVR/_icf_127/service/incidents.c$line_of_business/service"
            htmlPage = """<html><head>
            <meta property="og:title" content="Hollister Facebook Messenger Chat" />
            <meta property="og:image" content="/hollister/favicon-32x32.png" />
            <meta property="og:description" content="Hollister Facebook Messenger Chat" />
            <meta property="og:url" content="http://www.hlp.chat/Hollister" />
            <meta property="og:site-name" content="Hollister Facebook Messenger Chat"/>
            <meta property="og:type" content="website" />
            <Title>Hollister Facebook Messenger Chat</Title>    
            <link rel="apple-touch-icon" sizes="57x57" href="/hollister/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="/hollister/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="/hollister/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="/hollister/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="/hollister/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="/hollister/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="/hollister/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="/hollister/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="/hollister/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="/hollister/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/hollister/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="/hollister/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/hollister/favicon-16x16.png">
            <link rel="manifest" href="/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/hollister/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <script>window.location = '%s';</script></head><body></body></html>""" % redirectURL

            self.response.write(htmlPage)

        elif(url_path == "/HCo"):
            
            redirectURL = "https://www.hollisterco.com/shop/CustomerService?pageName=texts-terms&storeId=10251&textKey=LEGAL_TEXTS_TERMS&catalogId=10201&langId=-1"

        elif(url_path == "/Soothe"):
           redirectURL = "https://home-c30.incontact.com/inContact/ChatClient/ChatClient.aspx?poc=650eea82-3413-4376-bb75-a5ef85005435&bu=4596805&P1=First Name&P2=Last Name&P3=first.last@company.com&P4=555-555-5555"
        elif(url_path == "/Curvy"):
#           redirectURL = "https://lc.chat/now/5349091/"



            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                        <head><title>Curvy Chat Support</title></head><body>


                    <center><a href="https://www.curvy.com.au"><img style="margin-top:200px;width:300px" src="curvy/curvy_logo.png"></a></center>

<!-- HelpFlow website chat service start -->
<script type="text/javascript">
var __lc = {};
__lc.license = 5349091;
__lc.group = "121";
__lc.ga_version = "ga";
__lc.chat_between_groups = false;   

__lc.params = [
{ name: '!!!!PLATFORM BETA CUSTOMER!!!!', value: 'http://my.helpflow.net/agents/faqs/' },
{ name: 'Customer', value: 'www.curvy.com.au' },
{ name: 'Customer Code', value: 'CURV' },
{ name: 'Customer Brief', value: 'http://goo.gl/iQKHUf' },
{ name: 'Site Search', value: 'http://goo.gl/58MJYT' },
{ name: 'Product', value: 'http://goo.gl/DIy84E' },
{ name: 'Contact Page', value: 'http://goo.gl/QvA8rU' }
];

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);

})();

</script>
<script type="text/javascript">
var sc_project=11096645;
var sc_invisible=1; 
var sc_security="b1feae47"; 
var scJsHost = (("https:" == document.location.protocol) ? "https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +scJsHost+"statcounter.com/counter/counter.js'></"+"script>");

var LC_API = LC_API || {};
LC_API.on_after_load = function() {
  LC_API.open_chat_window();
};


</script>

    
            </body></html>"""
            
            self.response.write(htmlPage)



        elif(url_path == "/PrettyLitter"):
            #htmlPage = GetPrettyLitterHTML()
            #self.response.write(htmlPage)

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Chat Pretty Litter - Cat Litter That Protects Your Cat</title>

<!-- Start of PrettyLitter Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=639aeb37-4e68-4cea-8f8f-1761b2110f3a"> </script>
<!-- End of PrettyLitter Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.prettylittercats.com"><img src='/prettylitter/prettylitter_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#041E42;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>


 
                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)
            
        elif(url_path == "/EverythingKitchens"):
           redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4VEUzv8zNVfnUr1JPXwJMUgDJWGBavIZ"            
        elif(url_path == "/DollarShaveClub"):
           redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4cFETiUzIIMbxeFsnNjlgC7kYEooo3g0"            
        elif(url_path == "/OrchardMile"):

            htmlPage = """

<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
<title>Orchard Mile Support</title>
<script src="https://wchat.freshchat.com/js/widget.js"></script>
</head>
<body>
<center><a href="http://www.orchardmile.com"><img style="margin-top:200px" src="/orchardmile/orchardmile-logo.png"></a></center>

<script>
  window.fcWidget.init({
    token: "32559194-c828-4593-80ef-7685510299e1",
    host: "https://wchat.freshchat.com"
  });
</script>

</body>
</html>


            """
            
            self.response.write(htmlPage)



        elif(url_path == "/FourSigmatic"):
           redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4CJdCaIEe0uLHzVHo1TbWxAiNT1b2uQH"

        elif(url_path == "/Papier"):
           # redirectURL = "http://v2.zopim.com/widget/livechat.html?key=3CjdVBAqEx3nMyRT2NTkESABPqRYxKG9"
           redirectURL = "https://wa.me/441164647180" 

        elif(url_path == "/Ellesse2"):

              htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Ellesse Support</title>

<!-- Start of ellesse Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=c3c86440-504c-49e8-a582-13efd66cc9a1"> </script>
<!-- End of ellesse Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.ellesse.com"><img src='/ellesse/ellesse_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#041E42;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>


 
                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    // Hide the Drop-down list option in the Pre-Chat Form to select the Department
 //                                   $zopim.livechat.departments.filter('');

                                    // Automatically set the Department
//                                    $zopim.livechat.departments.setVisitorDepartment('Fossil eCom');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

              self.response.write(htmlPage)


        elif(url_path == "/Ellesse"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Ellesse Support</title>

<!-- Start of Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="ellesse.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.ellesse.com"><img src='/ellesse/ellesse_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#1F415E;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){

                            window.zESettings = {
                              webWidget: {
                                helpCenter: {
                                  suppress: false
                                }
                              }
                            };


                        
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.button.setPositionMobile('bl');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/EnduraSport"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Endura Sport Support</title>

<!-- Start of Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="endurasport.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/</script>
<!-- End of Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.endurasport.com"><img src='/endura_sport/endura_sport_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#000000;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){

                            window.zESettings = {
                              webWidget: {
                                helpCenter: {
                                  suppress: false
                                }
                              }
                            };


                        
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.button.setPositionMobile('bl');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/EnduraReturns"):
            redirectURL = 'https://cristisilva.typeform.com/to/p5pLTa'

        elif(url_path == "/SeaVees"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>SeaVees</title>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <style>
                    body {

                        text-align: center;
                    }
                    </style>
                    </head>
                    <body>

                <!--Gorgias Chat Widget Start-->
                    <script id="gorgias-chat-widget-install-v2" src="https://config.gorgias.chat/gorgias-chat-bundle-loader.js?applicationId=11528"></script>
                  <!--Gorgias Chat Widget End-->


                </script>
                <center>
                    <p><a href="http://www.seavees.com"><img src='/seavees/seavees_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                    <span style="color:white;background-color:#1F415E;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open();"> Chat with us </span>
                    
                </center>   


                <script>

                $(window).on('load', function() {

                setTimeout(
                  function() 
                  {$("div").trigger('click');}, 1000);});
                  

                </script>


                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/Paga"):
            redirectURL = "http://www.m.me/mypaga"
        elif(url_path == "/Tuckernuck"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=25gr077C8FC0QxBIcaA9v9WlX332BpSN"
        elif(url_path == "/TheBouqs"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=K9aBwLhJolvQF6WUCrCXhHB2u4FniLAq"
        elif(url_path == "/BellaElla"):
            redirectURL = "http://go.crisp.chat/chat/embed/?website_id=7b085054-dbd6-4b2a-9942-a03ba13a96c1"
        elif(url_path == "/Organifi"):
            
            htmlPage = """<html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
<!--Start 8x8 Chat Script -->
    <style>
    div#chatfloat356 {

        position: fixed;
        top: 50%;
        left: 80px;
	}
    </style>
    <center>
    <div id="chatfloat356">
        <!-- Place this code snippet anywhere you want the button to appear in your page. If no button has been configured in the chat script, it will not show up nor take any space. -->
        <div id="__8x8-chat-button-container-script_20896556225a04b881dce734.00292596"></div>

        <!-- This script will not interfere with the button layout, you just need to include it in the same page. It must also be within the <body> section of the page, preferably just before the ending tag. -->
        <script type="text/javascript">
           window.__8x8Chat = {
               uuid: "script_20896556225a04b881dce734.00292596",
               tenant: "b3JnYW5pZmkwMQ",
               channel: "Chat with us!",
               domain: "https://vcc-na11.8x8.com",
               path: "/.",
               buttonContainerId: "__8x8-chat-button-container-script_20896556225a04b881dce734.00292596",
               align: "right"
           };

           (function() {
               var se = document.createElement("script");
               se.type = "text/javascript";
               se.async = true;
               se.src = window.__8x8Chat.domain + window.__8x8Chat.path + "/CHAT/common/js/chat.js";

               var os = document.getElementsByTagName("script")[0];
               os.parentNode.insertBefore(se, os);
           })();
        </script>
        <script>

$(window).on('load', function() {

setTimeout(
  function() 
  {$("img").trigger('click');}, 1000);});
  

</script>
    </div>
    </center>
<!--End 8x8 Chat Script-->                
                </body>
                </html>"""
            
            self.response.write(htmlPage)

        # elif(url_path == "/VeraBradley"):

        #     redirectURL = "https://verabradley.com/pages/contact-us"

        
        elif(url_path == "/VeraBradley"):

            self.response.headers['Content-Security-Policy'] = "frame-ancestors 'self' *.vergic.com"
            self.response.headers['X-Frame-Options'] = 'ALLOW-FROM *.vergic.com'


            htmlPage = """

<html>
<head><meta name="viewport" content="width=device-width, initial-scale=1">
<title>Vera Bradley chat</title>
</head>
<body>

    <div style="padding-top:200px"><center><a href="https://www.verabradley.com"><img width="230px" src="verabradley/verabradleylogo.png"></a></center></div>

    <!-- START VERGIC ENGAGE SCRIPT -->
    <script type="text/javascript">
        
        (function(server,psID){
         var s=document.createElement('script');
         s.type='text/javascript';
         s.src=server+'/'+psID+'/ps.js';
         document.getElementsByTagName('head')[0].appendChild(s);
         }('https://us-content.vergic.com','2CCEEB85-AE2E-4B3C-A7A6-F66F4A1E4E81'));
    


        setTimeout(function(){
                        vngage.set("debug",true);
                    }, 500);
    </script>
    
        <!-- END VERGIC ENGAGE SCRIPT -->
</body>
</html>
            """

            self.response.write(htmlPage)

        # elif(url_path in ['/VeraBradley']):
        #     redirectURL = 'https://verabradley.com/pages/contact-us'

        # elif('verabradley' in url_path.lower()):
        #     redirectURL = 'https://verabradley.com/pages/contact-us'



        elif(url_path == "/KidGuard" or url_path == "/kidguard"):

            htmlPage = """

<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
<title>KidGuard Support</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<center><a href="http://www.kidguard.com"><img style="margin-top:200px" src="/kidguard/kidguard-logo.png"></a></center>

<script type='text/javascript'>
var path = window.location.pathname;
var noLiveChat = ['/lp/dsk/012/b/'];

if (location.pathname.split("/")[2] == 'mbl' || location.pathname.split("/")[1] == 'people-search' || noLiveChat.indexOf(path) > -1) {
  console.log('No LiveChat');
} else {

var fc_CSS=document.createElement('link');fc_CSS.setAttribute('rel','stylesheet');var fc_isSecured = (window.location && window.location.protocol == 'https:');var fc_lang = document.getElementsByTagName('html')[0].getAttribute('lang'); var fc_rtlLanguages = ['ar','he']; var fc_rtlSuffix = (fc_rtlLanguages.indexOf(fc_lang) >= 0) ? '-rtl' : '';fc_CSS.setAttribute('type','text/css');fc_CSS.setAttribute('href',((fc_isSecured)? 'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets1.chat.freshdesk.com')+'/css/visitor'+fc_rtlSuffix+'.css');document.getElementsByTagName('head')[0].appendChild(fc_CSS);var fc_JS=document.createElement('script'); fc_JS.type='text/javascript'; fc_JS.defer=true;fc_JS.src=((fc_isSecured)?'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets.chat.freshdesk.com')+'/js/visitor.js';(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS);window.livechat_setting= 'eyJ3aWRnZXRfc2l0ZV91cmwiOiJvbnJhbXBsYWIuZnJlc2hkZXNrLmNvbSIsInByb2R1Y3RfaWQiOm51bGwsIm5hbWUiOiJPbnJhbXBsYWIiLCJ3aWRnZXRfZXh0ZXJuYWxfaWQiOm51bGwsIndpZGdldF9pZCI6ImQ4OTdmN2ZmLWFjYzItNGNkMS05OThhLTgxNDQxMTYyZWU2ZSIsInNob3dfb25fcG9ydGFsIjpmYWxzZSwicG9ydGFsX2xvZ2luX3JlcXVpcmVkIjpmYWxzZSwibGFuZ3VhZ2UiOiJlbiIsInRpbWV6b25lIjoiRWFzdGVybiBUaW1lIChVUyAmIENhbmFkYSkiLCJpZCI6MzMwMDAwNDkyMTAsIm1haW5fd2lkZ2V0IjoxLCJmY19pZCI6IjJlYjIxNmQ3OTMzOTc3OWZhZjIyNGUzYzY1YzcwNGI3Iiwic2hvdyI6MSwicmVxdWlyZWQiOjIsImhlbHBkZXNrbmFtZSI6Ik9ucmFtcGxhYiIsIm5hbWVfbGFiZWwiOiJOYW1lIiwibWVzc2FnZV9sYWJlbCI6Ik1lc3NhZ2UiLCJwaG9uZV9sYWJlbCI6IlBob25lIiwidGV4dGZpZWxkX2xhYmVsIjoiVGV4dGZpZWxkIiwiZHJvcGRvd25fbGFiZWwiOiJEcm9wZG93biIsIndlYnVybCI6Im9ucmFtcGxhYi5mcmVzaGRlc2suY29tIiwibm9kZXVybCI6ImNoYXQuZnJlc2hkZXNrLmNvbSIsImRlYnVnIjoxLCJtZSI6Ik1lIiwiZXhwaXJ5IjoxNTA2MzI1NzU1MDAwLCJlbnZpcm9ubWVudCI6InByb2R1Y3Rpb24iLCJlbmRfY2hhdF90aGFua19tc2ciOiJUaGFuayB5b3UhISEiLCJlbmRfY2hhdF9lbmRfdGl0bGUiOiJFbmQiLCJlbmRfY2hhdF9jYW5jZWxfdGl0bGUiOiJDYW5jZWwiLCJzaXRlX2lkIjoiMmViMjE2ZDc5MzM5Nzc5ZmFmMjI0ZTNjNjVjNzA0YjciLCJhY3RpdmUiOjEsInJvdXRpbmciOm51bGwsInByZWNoYXRfZm9ybSI6MSwiYnVzaW5lc3NfY2FsZW5kYXIiOm51bGwsInByb2FjdGl2ZV9jaGF0IjowLCJwcm9hY3RpdmVfdGltZSI6bnVsbCwic2l0ZV91cmwiOiJvbnJhbXBsYWIuZnJlc2hkZXNrLmNvbSIsImV4dGVybmFsX2lkIjpudWxsLCJkZWxldGVkIjowLCJtb2JpbGUiOjEsImFjY291bnRfaWQiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDE3LTA4LTI1VDA4OjA2OjU1LjAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAxNy0wOC0zMVQwOTozMjozMi4wMDBaIiwiY2JEZWZhdWx0TWVzc2FnZXMiOnsiY29icm93c2luZ19zdGFydF9tc2ciOiJZb3VyIHNjcmVlbnNoYXJlIHNlc3Npb24gaGFzIHN0YXJ0ZWQiLCJjb2Jyb3dzaW5nX3N0b3BfbXNnIjoiWW91ciBzY3JlZW5zaGFyaW5nIHNlc3Npb24gaGFzIGVuZGVkIiwiY29icm93c2luZ19kZW55X21zZyI6IllvdXIgcmVxdWVzdCB3YXMgZGVjbGluZWQiLCJjb2Jyb3dzaW5nX2FnZW50X2J1c3kiOiJBZ2VudCBpcyBpbiBzY3JlZW4gc2hhcmUgc2Vzc2lvbiB3aXRoIGN1c3RvbWVyIiwiY29icm93c2luZ192aWV3aW5nX3NjcmVlbiI6IllvdSBhcmUgdmlld2luZyB0aGUgdmlzaXRvcuKAmXMgc2NyZWVuIiwiY29icm93c2luZ19jb250cm9sbGluZ19zY3JlZW4iOiJZb3UgaGF2ZSBhY2Nlc3MgdG8gdmlzaXRvcuKAmXMgc2NyZWVuLiIsImNvYnJvd3NpbmdfcmVxdWVzdF9jb250cm9sIjoiUmVxdWVzdCB2aXNpdG9yIGZvciBzY3JlZW4gYWNjZXNzICIsImNvYnJvd3NpbmdfZ2l2ZV92aXNpdG9yX2NvbnRyb2wiOiJHaXZlIGFjY2VzcyBiYWNrIHRvIHZpc2l0b3IgIiwiY29icm93c2luZ19zdG9wX3JlcXVlc3QiOiJFbmQgeW91ciBzY3JlZW5zaGFyaW5nIHNlc3Npb24gIiwiY29icm93c2luZ19yZXF1ZXN0X2NvbnRyb2xfcmVqZWN0ZWQiOiJZb3VyIHJlcXVlc3Qgd2FzIGRlY2xpbmVkICIsImNvYnJvd3NpbmdfY2FuY2VsX3Zpc2l0b3JfbXNnIjoiU2NyZWVuc2hhcmluZyBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUgIiwiY29icm93c2luZ19hZ2VudF9yZXF1ZXN0X2NvbnRyb2wiOiJBZ2VudCBpcyByZXF1ZXN0aW5nIGFjY2VzcyB0byB5b3VyIHNjcmVlbiAiLCJjYl92aWV3aW5nX3NjcmVlbl92aSI6IkFnZW50IGNhbiB2aWV3IHlvdXIgc2NyZWVuICIsImNiX2NvbnRyb2xsaW5nX3NjcmVlbl92aSI6IkFnZW50IGhhcyBhY2Nlc3MgdG8geW91ciBzY3JlZW4gIiwiY2Jfdmlld19tb2RlX3N1YnRleHQiOiJZb3VyIGFjY2VzcyB0byB0aGUgc2NyZWVuIGhhcyBiZWVuIHdpdGhkcmF3biAiLCJjYl9naXZlX2NvbnRyb2xfdmkiOiJBbGxvdyBhZ2VudCB0byBhY2Nlc3MgeW91ciBzY3JlZW4gIiwiY2JfdmlzaXRvcl9zZXNzaW9uX3JlcXVlc3QiOiJBZ2VudCBzZWVrcyBhY2Nlc3MgdG8geW91ciBzY3JlZW4gIn19';
}

            

                    $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {
				$("div").trigger("click");}, 800);
			});

                    </script>


</script>
</body>
</html>


            """
            
            self.response.write(htmlPage)



        elif(url_path[:8] == "/Minted/"):

            hashid = url_path[8:]
            htmlPage = GetMintedHTML(hashid)
            self.response.write(htmlPage)
#            redirectURL = "https://31ll.la1-c1-ord.salesforceliveagent.com/content/s/chat?language=#deployment_id=57240000000CbAm&org_id=00D40000000Mx87&button_id=57340000000CbJ7"            
        elif(url_path == "/Minted"):
            redirectURL = "https://c.la1-c1-iad.salesforceliveagent.com/content/s/chat?language=#deployment_id=57240000000CbAm&org_id=00D40000000Mx87&button_id=573f30000008PRO"
        elif(url_path == "/GentlemanClothing"):

            htmlPage = """

                    <html><meta name="viewport" content="width=device-width, initial-scale=1">
                    <body>

                    <!--Start of Tawk.to Script-->
                    <script type="text/javascript">
                    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                    (function(){
                    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                    s1.async=true;
                    s1.src='https://embed.tawk.to/widget-script/5a13b1fb198bd56b8c03c55c/1bvefok57.js?shop=gentlemen-clothing.myshopify.com';
                    s1.charset='UTF-8';
                    s1.setAttribute('crossorigin','*');
                    s0.parentNode.insertBefore(s1,s0);
                    })();

                    Tawk_API.onLoad = function(){
                        Tawk_API.maximize();
                    };
                    
                    </script>
                    <!--End of Tawk.to Script-->



                    </body>
                    </html>

                    """
            
            self.response.write(htmlPage)


        elif(url_path == "/Fossil"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Fossil Support</title>

                        <!-- Start of chatdesk Zendesk Widget script -->
                    <script type="text/javascript" id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=eb6b79dd-47ee-466f-96e5-ba77244e6495"> </script>
                <!-- End of chatdesk Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.fossil.com"><img src='/fossil/fossil_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:black;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>


 
                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    // Hide the Drop-down list option in the Pre-Chat Form to select the Department
                                    $zopim.livechat.departments.filter('');

                                    // Automatically set the Department
                                    $zopim.livechat.departments.setVisitorDepartment('Fossil eCom');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/FossilOrderStatus"):
           redirectURL = "https://www.fossil.com/us/en/customer-care/order-status.html"

# GrooveLife Paths
        
        elif(url_path == "/GrooveLife"):

            htmlPage = """
                <html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Groove Life</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
                <style>

                body {

                    text-align: center;
                }

                h2 {
                    font-family: "Lato", sans-serif;
                    font-weight: 800;
                    letter-spacing: 0.1em;
                    line-height: 1;
                    text-transform: uppercase;

                }

                img {
                    width:auto;
                    max-width:300px;
                    margin-bottom: 3.125rem;
                }

                a {
                    text-decoration: none;
                    color: inherit;
                }

                .wrapper {

                    padding-top: 10%;
                    margin: auto;
                    text-align: center;
                    vertical-align: middle;
                    padding-right: 1.5625rem;
                    padding-left: 1.5625rem;

                }

                .button_text {

                    padding: 0.9375rem 1.875rem;
                    margin: auto;
                    margin-bottom: 3.125rem;
                    width: 200px;

                    text-align: center;
                    color: #fff;
                    background-color:rgb(244, 123, 33);
                    
                    font-family: "Lato", sans-serif;
                    letter-spacing: 2px;
                    text-transform: uppercase;
                    font-weight: 800;
                    font-size: 0.875rem;
                    line-height: 1.25rem;

                    transition: all 0.25s;
                }

                .button_text:hover {
                    opacity: 0.6;
                }

                </style>
                </head>
                <body>
                <!-- Gorgias Chat Widget Start -->
                <div id="gorgias-chat">
                <script>window.gorgiasChatParameters = {}
                </script>
                <script src="https://config.gorgias.io/production/DrQqY7XJEl6L4Wjv/chat/Oro0V7LeRwx9d83N.js" defer>
                </script>
                </div>
                <!-- Gorgias Chat Widget End -->
                  

                </script>
                <div class="wrapper">
                    <img src="https://s3.amazonaws.com/returnlogic-media/production/b9884326-6bb0-405e-ae89-cfa19e970ffc/uploads/images/GL_main-logo_Black.png">

                <a>
                <div class="button_text" onclick="Smooch.open();">
                    LIVE CHAT WITH US
                </div>
                </a>

                <a href="https://m.me/GrooveLife.co">
                <div class="button_text">
                    MESSAGE US ON FACEBOOK
                </div>
                </a>
                </div>

                </body>
                </html>"""

            self.response.write(htmlPage)

# GrooveLife Warranty

        elif(url_path == "/GrooveLifeWarranty"):

            htmlPage = """
                <html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Groove Life</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
                <style>

                body {

                    text-align: center;
                    font-family: "Lato", sans-serif;

                }

                h2 {

                    font-family: Arial,sans-serif;
                    font-weight: 800;
                    font-size: 1.2rem;
                    letter-spacing: 0.1em;
                    text-transform: uppercase;
                    max-width: 500px;
                    margin: 0 auto 30px;

                }

                img {
                    width:auto;
                    max-width:300px;
                    margin-bottom: 1.5rem;
                }

                .button{
                    text-decoration: none;
                    color: inherit;
                }


                .wrapper {

                    padding-top: 10%;
                    margin: auto;
                    text-align: center;
                    vertical-align: middle;
                    padding-right: 1.5625rem;
                    padding-left: 1.5625rem;

                }

                .lower_text {
                    margin: 0 auto;
                    max-width: 400px;

                }


                .lower_text a {
                    margin: 0 auto;
                    max-width: 400px;
                    color: rgb(244, 123, 33);

                }

                .lower_text a:hover {
                    color: black;
                }

                .button_text {

                    padding: 0.9375rem 1.875rem;
                    margin: auto;
                    margin-bottom: 3.125rem;
                    width: 200px;

                    text-align: center;
                    color: #fff;
                    background-color:rgb(244, 123, 33);
                    
                    letter-spacing: 2px;
                    text-transform: uppercase;
                    font-weight: 800;
                    font-size: 0.875rem;
                    line-height: 1.25rem;

                    transition: all 0.25s;
                }

                .button_text:hover {
                    opacity: 0.6;
                }

                </style>
                </head>
                <body>

                <div class="wrapper">
                    <img src="/groovelife/groovelife_logo.png">
                    
                    <h2>Did you purchase your ring from one of our retail partners?</h2>
                    <a class="button" href="https://groovelife.com/pages/nobs">
                    <div class="button_text">
                        REGISTER YOUR RING
                    </div>
                    </a>

                    <h2>Did you buy online at groovelife.com?</h2>
                    <a class="button" href="https://groovelife.returnlogic.com/">
                    <div class="button_text">
                        START YOUR WARRANTY REPLACEMENT
                    </div>
                    </a>

                    <div class="lower_text">*If your ring was lost, please contact us on <a href="/GrooveLife">live chat</a>, <a href="https://m.me/GrooveLife.co">Facebook Messenger</a>, or you can send an email to <a href="mailto:support@groovelife.com">support@groovelife.com</a></div>
                </div>

                </body>
                </html>"""

            self.response.write(htmlPage)

        elif(url_path == "/GrooveLifeChat"):

            htmlPage = """<html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Groove Life</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <style>
                body {

                    text-align: center;
                }

                img {
                    width: 200px;
                    padding-top: 15%;

                }
                </style>
                </head>
                <body>

                <!-- Gorgias Chat Widget Start --><div id="gorgias-chat"><script>window.gorgiasChatParameters = {}</script><script src="https://config.gorgias.io/production/DrQqY7XJEl6L4Wjv/chat/Oro0V7LeRwx9d83N.js" defer></script></div><!-- Gorgias Chat Widget End -->


                </script>
                <img style="width:500px" src="/groovelife/groovelife_logo.png">


                <div onclick="Smooch.open();"></div>

                        <script>

                $(window).on('load', function() {

                setTimeout(
                  function() 
                  {$("div").trigger('click');}, 1000);});
                  

                </script>


                </body>
                </html>"""
            
            self.response.write(htmlPage)

### AutoAnything
        elif(url_path == "/AutoAnything"):
            htmlPage = """
                <html>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                                    
                <head>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <style>
                 body {

                    text-align: center;
                    font-family: "Lato", sans-serif;

                }
                .rotated { 
                    -webkit-transform: rotate(90deg);
                    -moz-transform: rotate(90deg);
                    -o-transform: rotate(90deg);
                    -ms-transform: rotate(90deg);
                    transform: rotate(90deg);
                }
                </style>

                </head>
                <body>
                        <a href="http://www.autoanything.com">
                        <img src='autoanything/aalogo2.png' style='max-width:300px;margin-top:170px;margin-bottom:40px;text-align:center;'></a>
                    <div class="boldChatRightStatic">
                        <!-- BoldChat Live Chat Button HTML v5.00 (Type=Web,ChatButton=xxx Live Chat Button - Layered Form,Website=www.autoanything.com) -->
                        <div style="text-align: center; white-space: nowrap;">
                            <script type="text/javascript">var bccbId=Math.random();document.write(unescape('%3Cdiv id='+bccbId+'%3E%3C/div%3E'));window._bcvma=window._bcvma||[];_bcvma.push(["setAccountID","4474476142785323378"]);_bcvma.push(["setParameter","WebsiteID","3878381532623136004"]);_bcvma.push(["addStatic",{type:"chat",bdid:"4391456276393702638",id:bccbId}]);var bcLoad=function(){if(window.bcLoaded)return;window.bcLoaded=true;var vms=document.createElement("script");vms.type="text/javascript";vms.async=true;vms.src=('https:'==document.location.protocol?'https://':'http://')+"vmss.boldchat.com/aid/4474476142785323378/bc.vms4/vms.js";var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(vms,s);};if(window.pageViewer&&pageViewer.load)pageViewer.load();else if(document.readyState=="complete")bcLoad();else if(window.addEventListener)window.addEventListener('load',bcLoad,false);else window.attachEvent('onload',bcLoad);
                        </script>
                    </div>
                        <!-- /BoldChat Live Chat Button HTML v5.00 -->
                    </div>

                    <span style="color:white;background-color:#1F415E;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us 
                    </span>
                            <script type="text/javascript">
                            $(window).on('load', function() {

  

                            # setTimeout(
                            #   function() 
                            #   {
                            #   $("img:eq(1)").trigger('click');}, 1000);});
                              

                            </script>
                </body>
                </html>
                """


            self.response.write(htmlPage)


        elif(url_path == "/DrHos"):

            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                <head><title>Dr. Ho's</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                <!-- Gorgias Chat Widget Start --><div id="gorgias-chat"><script>window.gorgiasChatParameters = {}</script><script src="https://config.gorgias.io/production/zKB3oxwqPg2rkVOL/chat/5k0Dw70r0g6voQAM.js" defer></script></div><!-- Gorgias Chat Widget End -->


                </script>


                <div onclick="Smooch.open();"></div>

                        <script>

                $(window).on('load', function() {

                setTimeout(
                  function() 
                  {$("div").trigger('click');}, 1000);});
                  

                </script>


                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path == "/GrooveLifeEmail"):

            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Groove Life Support</title>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.groovelife.com"><img src='/groovelife/groovelife_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <a id="email_link" href="mailto:support@groovelife.com" style="text-decoration:none;"><span style="color:white;background-color:#F47B20;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer"> Email us </span></a>                        
                    </center>
                </body>
            </html>"""

            self.response.write(htmlPage)

        elif(url_path == "/TheGrommetCatalog"):

            redirectURL = "https://www.thegrommet.com/customer-service/contact"
            
        elif(url_path == "/Freshly"):

              htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Freshly Support</title>

                        <!-- Start of chatdesk Zendesk Widget script -->
                    <script type="text/javascript" id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=c86c312d-51c6-4800-a664-65b6a998af4a"> </script>
                    <script src="https://v2.zopim.com/?6KVETGhtgvVs2QIysISvvNHpyqylt4mE"></script>
                <!-- End of chatdesk Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.freshly.com"><img src='/freshly/freshly_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#268C59;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>


 
                    
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    // Hide the Drop-down list option in the Pre-Chat Form to select the Department
 //                                   $zopim.livechat.departments.filter('');

                                    // Automatically set the Department
//                                    $zopim.livechat.departments.setVisitorDepartment('Fossil eCom');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>



            """

              self.response.write(htmlPage)
            
        elif(url_path == "/CabinetParts" or url_path == "/CabinetPartsOrders" or url_path == "/KeatsCastle"):

            redirectURL = "https://wksxthob23x.typeform.com/c/JEIsuksY"
                
            
        elif(url_path == "/Chatdesk"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4KwuFB29oNDCVGYojSXHAubN0MpS7iZF"            
            

        elif(url_path == "/VegaReturns"):

            redirectURL = "https://goo.gl/forms/tmVDA73tBj0xS0Pt1"                        

        elif(url_path == "/Vega"):
        
            htmlPage = GetVegaHTML()
            self.response.write(htmlPage)
        
        elif(url_path == "/OneHopeWine" or url_path == "/onehopewine"):
            
            htmlPage = """

                    <html><meta name="viewport" content="width=device-width, initial-scale=1">
                    <body>
                    <head>
                    <title>OneHope Wine Chat</title>
                                        <script type="text/javascript">!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});</script>
                    <script type="text/javascript">window.Beacon('init', '9946cd9f-5bc5-400b-8899-57f74f999f71');
                    window.Beacon("open");

                    Beacon('config', {
                      "display": {
                        "position": "right",
                      }
                    });
                    
                    </script>

                    </head>
                    <body>
                    <center><img style="margin-top:200px" src="/onehope/onehope-logo.jpg"></center>
                    </body>
                    </html>


                    """

#                    <!-- Start of HubSpot Embed Code --> <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4349308.js"></script> <!-- End of HubSpot Embed Code -->

            
            self.response.write(htmlPage)
        
        elif(url_path == "/FelixGrayFeedback"):
            redirectURL = "https://shopfelixgray.wufoo.com/forms/w1trzyd704r02ra/"

        elif(url_path == "/FelixGrayEmail"):

            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Felix Gray</title>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.felixgray.com"><img src='/felixgray/felix_gray_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <a id="email_link" target="blank" href="mailto:help@felixgray.com?subject=I'd%20like%20to%20exchange%20a%20gift&body=Hi there,%0D%0AI'd like to perform an exchange for a gift.%0D%0A%0D%0A(Fill out one of the two fields below)%0D%0AOrder Number: [#]%0D%0AOR%0D%0A%0D%0APurchaser's email: [EMAIL]%0D%0A%0D%0AThe recipient's mailing address is:%0D%0A[NAME]%0D%0A[ADDRESS]%0D%0A%0D%0AI would like to exchange for [DESIRED GLASSES STYLE] in [DESIRED GLASSES COLOR]%0D%0A%0D%0AThanks!%0D%0A%0D%0A[Your name]" style="text-decoration:none;"><span style="color:white;background-color:#094e5a;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer"> Email us </span></a>                        
                    </center>
                </body>
            </html>"""

            self.response.write(htmlPage)

                
        elif(url_path == "/Unpakt"):
            
            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                <body>
                <!-- begin olark code -->
                <script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
                /* custom configuration goes here (www.olark.com/documentation) */
                olark.identify('8990-941-10-9935');</script>
                <!-- end olark code -->
                
                <script type="text/javascript">
                function openOlark() {
                olark('api.box.expand');
                }
                window.onload = openOlark;
                
                </script>
                
                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path == "/BeachBunny"):
                

            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                <head><title>Beach Bunny</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>



                <center>
                        <p><a href="https://www.beachbunnyswimwear.com/"><img src='/beachbunny/beach_bunny_logo.jpg' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#e91d74;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open();"> Chat with us </span>
                        
                    </center>

                    <script>!function(_){_.GORGIAS_CHAT_APP_ID="1284",_.GORGIAS_CHAT_BASE_URL="us-east1-898b.production.gorgias.chat",_.GORGIAS_API_BASE_URL="config.gorgias.chat";var e=new XMLHttpRequest;e.open("GET","https://config.gorgias.chat/applications/1284",!0),e.onload=function(n){if(4===e.readyState)if(200===e.status){var t=JSON.parse(e.responseText);if(!t.application||!t.bundleVersion)throw new Error("Missing fields in the response body - https://config.gorgias.chat/applications/1284");if(_.GORGIAS_CHAT_APP=t.application,_.GORGIAS_CHAT_BUNDLE_VERSION=t.bundleVersion,t&&t.texts&&(_.GORGIAS_CHAT_TEXTS=t.texts),!document.getElementById("gorgias-chat-container")){var o=document.createElement("div");o.id="gorgias-chat-container",document.body.appendChild(o);var r=document.createElement("script");r.setAttribute("defer",!0),r.src="https://storage.googleapis.com/gorgias-chat-production-client-builds/{bundleVersion}/static/js/main.js".replace("{bundleVersion}",t.bundleVersion),document.body.appendChild(r)}}else console.error("Failed request GET - https://config.gorgias.chat/applications/1284")},e.onerror=function(_){console.error(_)},e.send()}(window||{});</script>
      <script>!function(n){n.IS_SHOPIFY=!0,n.SHOPIFY_PERMANENT_DOMAIN="beachbunnyswimwear.myshopify.com",n.SHOPIFY_CUSTOMER_ID=""}(window||{});</script><!--Gorgias Chat Widget End-->

                <div onclick="GorgiasChat.open();"></div>

                        <script>

                $(window).on('load', function() {

                setTimeout(
                  function() 
                  {$("div").trigger('click');}, 1000);});
                  

                </script>


                </body>
                </html>"""
            
            self.response.write(htmlPage)
        
        
        elif(url_path == "/Bedgear"):
            
            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
                <body>
                <!-- begin olark code -->
                <script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
                /* custom configuration goes here (www.olark.com/documentation) */
                olark.identify('3783-403-10-1319');</script>
                <!-- end olark code -->
                
                <script type="text/javascript">
                function openOlark() {
                olark('api.box.expand');
                }
                window.onload = openOlark;
                </script>
                
                </body>
                </html>"""
            
            self.response.write(htmlPage)
        
        elif(url_path == "/Dessy"):
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "vwh7z41z"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('show');</script>
                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path[:7] == "/Dessy/"):

            from hashids import Hashids
            hashids = Hashids()
            cur_hashid = url_path[7:]            
            customer_phone_number = hashids.decode(cur_hashid)[0]
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "vwh7z41z",
                phone: "%s"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('show');</script>
                </body>
                </html>""" % (customer_phone_number)
            
            self.response.write(htmlPage)

        
        elif(url_path == "/GenerationTux"):
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "wqqurx1x"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('show');</script>
                </body>
                </html>"""
            
            self.response.write(htmlPage)
        
        elif(url_path == "/Whistle"):

            if "iPhone" in self.request.headers["User-Agent"] or "iPad" in self.request.headers["User-Agent"]:

                redirectURL = "https://apps.apple.com/us/app/whistle-pet-tracker/id1182564185"

            else:

                redirectURL = "https://play.google.com/store/apps/details?id=com.whistle.bolt&hl=en_US"


        elif(url_path == "/Whistle2"):
            htmlPage = """<html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Whistle</title>
                <style>
                body {

                    text-align: center;
                }

                img {
                    width: 500px;
                    padding-top: 15%;

                }
                </style>
                </head>
                <body>

                <img src="https://assets-global.website-files.com/5cf972829c5f0511e15517e2/5d2fd493e88321a77810f146_whistle-logo-grey-min.png">


                </body>
                </html>"""


            self.response.write(htmlPage)


        elif(url_path == "/Apliiq"):

            redirectURL = "https://salesiq.zoho.com/apliiq/drawchat.ls?&type=float&referrer=&pagetitle=&embedname=apliiq&cpage=&currdomain=hlp.chat&autochat=false&lang=en&loadedtime=&isnewwindow=true"

        elif(url_path == "/Farmgirl"):

            htmlPage = """

<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head><title>Farmgirl Flowers Support</title></head>
<body>
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement('iframe');r.setAttribute('id', 'zdFrame');window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="farmgirlflowerssupport.zendesk.com",this.zEQueue=a,this.body.appendChild(e);},o.write('<body onload="document._l();">'),o.close()}();
/*]]>*/

window.setTimeout(function(){window.zE.activate();}, 800);
</script>
<center><br><p><a href="http://www.farmgirlflowers.com"><img src="farmgirl/farmgirl_logo.png"></a></p><div onclick="window.zE.activate();" style="margin-top:40px;cursor:pointer;padding:20px;background-color:#4ABAB4;width:200px;font-size:20px;font-weight:bold;box-shadow: 1px 1px #DCDCDC;border-radius: 10px;color:white;font-family:'Arial';">Support</div></center>

</body>
</html>

            """

            self.response.write(htmlPage)

        elif(url_path == "/InspiredStart"):

            htmlPage = """

<!DOCTYPE html><meta name="viewport" content="width=device-width, initial-scale=1">
<html dir="ltr" lang="en-US">
    
    <head><title>Inspired Start: the only baby food designed for early allergen introduction</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
        <center>
        <a href="http://www.inspired-start.com"><img src='/inspiredstart/inspiredstart_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
        </center>


<script>"use strict";
!function() {
var t = window.driftt = window.drift = window.driftt || [];
if (!t.init) {
if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
t.factory = function(e) {
return function() {
var n = Array.prototype.slice.call(arguments);
return n.unshift(e), t.push(n), t;
};
}, t.methods.forEach(function(e) {
t[e] = t.factory(e);
}), t.load = function(t) {
var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
var i = document.getElementsByTagName("script")[0];
i.parentNode.insertBefore(o, i);
};
}
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('i9h8a37utrde');</script> 
<!-- place this script tag after the Drift embed tag -->
<script>
  (function() {
    /* Add this class to any elements you want to use to open Drift.
     *
     * Examples:
     * - <a class="drift-open-chat">Questions? We're here to help!</a>
     * - <button class="drift-open-chat">Chat now!</button>
     *
     * You can have any additional classes on those elements that you
     * would ilke.
     */
    var DRIFT_CHAT_SELECTOR = '.drift-open-chat'
    /* http://youmightnotneedjquery.com/#ready */
    function ready(fn) {
      if (document.readyState != 'loading') {
        fn();
      } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
      } else {
        document.attachEvent('onreadystatechange', function() {
          if (document.readyState != 'loading')
            fn();
        });
      }
    }
    /* http://youmightnotneedjquery.com/#each */
    function forEachElement(selector, fn) {
      var elements = document.querySelectorAll(selector);
      for (var i = 0; i < elements.length; i++)
        fn(elements[i], i);
    }
    function openSidebar(driftApi, event) {
      event.preventDefault();
      driftApi.sidebar.open();
      return false;
    }
    ready(function() {
      drift.on('ready', function(api) {
        var handleClick = openSidebar.bind(this, api)
        forEachElement(DRIFT_CHAT_SELECTOR, function(el) {
          el.addEventListener('click', handleClick);
        });
      });
    });
  })();
</script>

<!-- simply add 'class="drift-open-chat"' to any link on your site -->
  <a id="drift-chat-link-chatdesk" class="drift-open-chat" href="http://docs.drift.com/"></a>

        <script>

$(window).on('load', function() {

setTimeout(
  function() 
  {$("drift-chat-link-chatdesk").trigger('click');}, 800);});
  
</script>

</body>
</html>


            """

            self.response.write(htmlPage)

        elif(url_path == "/StealthBelt"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=hVR2ulkHgI0idCBP9hBrIwKSiLCs2CbL"
        elif(url_path == "/FelixGray"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4d1EiBAkwsbGW8IjYn946WvNdzu9y5rB"
        elif(url_path == "/Petcube"):

            self.response.headers['Access-Control-Allow-Origin'] = '*'
            self.response.headers['Access-Control-Allow-Origin'] = '*.my.salesforce.com'
            self.response.headers['Access-Control-Allow-Origin'] = '*.force.com'
            self.response.headers['Access-Control-Allow-Origin'] = '*.salesforce.com'


            htmlPage = """

<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head><title>Petcube chat</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

        <center>
        <a href="http://www.petcube.com"><img src='/petcube/petcube_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
        </center>

<script type="text/javascript" src="https://service.force.com/embeddedservice/5.0/esw.min.js"></script><script type="text/javascript">
var initESW = function(gslbBaseURL) {
embedded_svc.settings.displayHelpButton = true; //Or false
embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'

embedded_svc.settings.defaultMinimizedText = 'Start Chat'; //(Defaults to Chat with an Expert)
embedded_svc.settings.offlineSupportMinimizedText = 'Log a Case';
embedded_svc.settings.disabledMinimizedText = 'Log a Case'; //(Defaults to Agent Offline)

//embedded_svc.settings.loadingText = ''; //(Defaults to Loading)
//embedded_svc.settings.storageDomain = 'yourdomain.com'; //(Sets the domain for your deployment so that visitors can navigate subdomains during a chat session)

// Settings for Live Agent
//embedded_svc.settings.directToButtonRouting = function(prechatFormData) {
// Dynamically changes the button ID based on what the visitor enters in the pre-chat form.
// Returns a valid button ID.
//};
//embedded_svc.settings.prepopulatedPrechatFields = {}; //Sets the auto-population of pre-chat form fields
//embedded_svc.settings.fallbackRouting = []; //An array of button IDs, user IDs, or userId_buttonId
//embedded_svc.settings.offlineSupportMinimizedText = '...'; //(Defaults to Contact Us)

embedded_svc.settings.enabledFeatures = ['LiveAgent'];
embedded_svc.settings.entryFeature = 'LiveAgent';

embedded_svc.init('https://petcube.my.salesforce.com', 'https://petcube.secure.force.com', gslbBaseURL, '00D1N000002qchE', 'Live_Agent', { baseLiveAgentContentURL: 'https://c.la1-c1-iad.salesforceliveagent.com/content', deploymentId: '5721N000000ldBP', buttonId: '5731N000000lXec', baseLiveAgentURL: 'https://d.la1-c1-iad.salesforceliveagent.com/chat', eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04I1N000000kEJSUA2_164ff5b0d5b', isOfflineSupportEnabled: true}); };if (!window.embedded_svc) { var s = document.createElement('script'); s.setAttribute('src', 'https://petcube.my.salesforce.com/embeddedservice/5.0/esw.min.js'); s.onload = function() { initESW(null); }; document.body.appendChild(s); } else { initESW('https://service.force.com'); }</script>

        <script>

$(window).on('load', function() {

setTimeout(
  function() 
  {$("div").trigger('click');}, 1000);});
  

</script>


</body>
</html>

            """

            self.response.write(htmlPage)

        elif(url_path == "/LivingSpacesWarranty"):

            redirectURL = "https://www.livingspaces.com/help/warranty/claim"

        elif(url_path == "/LivingSpacesDelivery"):

            redirectURL = "https://www.livingspaces.com/track-delivery?utm_source=chatdesk"
            
        elif(url_path == "/LivingSpaces"):

            redirectURL = "https://www.livingspaces.com/chat?utm_source=chatdesk"

        elif(url_path == "/Feather"):
           
            htmlPage = """<html>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <head>
                    <title>Feather Support</title>
                    <script src="https://wchat.freshchat.com/js/widget.js"></script>
                    </head>
                    <body>
                    <center><a href="http://www.livefeather.com"><img style="margin-top:200px" src="/feather/feather_logo.png"></a></center>

                    <script>
                      window.fcWidget.init({
                        token: "6bd678c4-7850-4c78-a9b6-a8ca5810314a",
                        host: "https://wchat.freshchat.com"
                      });

                      function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
                     
                      window.fcWidget.user.setProperties({"Source": "Chatdesk"});

                      window.fcWidget.open();
                      window.fcWidget.show();
                    </script>

                    </body>
                    </html>
                """
           
            self.response.write(htmlPage)

        elif(url_path[:9] == "/Feather/"):

            from hashids import Hashids
            hashids = Hashids()
            cur_hashid = url_path[9:]            
            customer_phone_number = hashids.decode(cur_hashid)[0]
           
            htmlPage = """<html>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <head>
                    <title>Feather Support</title>
                    <script src="https://wchat.freshchat.com/js/widget.js"></script>
                    </head>
                    <body>
                    <center><a href="http://www.livefeather.com"><img style="margin-top:200px" src="/feather/feather_logo.png"></a></center>

                    <script>
                      window.fcWidget.init({
                        token: "6bd678c4-7850-4c78-a9b6-a8ca5810314a",
                        host: "https://wchat.freshchat.com"
                      });

                      function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);

                        // Copy the below lines under window.fcWidget.init inside initFreshChat function in the above snippet

                        // To set user phone and add phone as First Name and Chatdesk as last name
                        window.fcWidget.user.setPhone("%s");
                        window.fcWidget.user.setFirstName("%s");
                        window.fcWidget.user.setLastName("Chatdesk");

                     
                      window.fcWidget.user.setProperties({"Source": "Chatdesk"});

                      window.fcWidget.open();
                      window.fcWidget.show();

                     
                    </script>

                    </body>
                    </html>
                """ % (customer_phone_number,customer_phone_number)
           
            self.response.write(htmlPage)
            
        elif(url_path == "/MiniLuxeApp"):

            if "iPhone" in self.request.headers["User-Agent"] or "iPad" in self.request.headers["User-Agent"]:

                redirectURL = "https://apps.apple.com/us/app/miniluxe/id981106696"

            else:

                redirectURL = "https://play.google.com/store/apps/details?id=com.zenoti.miniluxe"

        elif(url_path == "/JaanuuOrderStatus"):

            redirectURL = "https://www.jaanuu.com/my-account/guest_order"

        elif(url_path == "/Agron"):

            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Agron</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="https://agron.io/"><img src='/agron/agron_logo.jpg' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#468725;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open()();"> Chat with us </span>
                        
                    </center>
                    
<!--Gorgias Chat Widget Start--><script id="gorgias-chat-widget-install">!function(_){_.GORGIAS_CHAT_APP_ID="3862",_.GORGIAS_CHAT_BASE_URL="us-east1-898b.production.gorgias.chat",_.GORGIAS_API_BASE_URL="config.gorgias.chat";var e=new XMLHttpRequest;e.open("GET","https://config.gorgias.chat/applications/3862",!0),e.onload=function(t){if(4===e.readyState)if(200===e.status){var n=JSON.parse(e.responseText);if(!n.application||!n.bundleVersion)throw new Error("Missing fields in the response body - https://config.gorgias.chat/applications/3862");if(_.GORGIAS_CHAT_APP=n.application,_.GORGIAS_CHAT_BUNDLE_VERSION=n.bundleVersion,n&&n.texts&&(_.GORGIAS_CHAT_TEXTS=n.texts),n&&n.sspTexts&&(_.GORGIAS_CHAT_SELF_SERVICE_PORTAL_TEXTS=n.sspTexts),!document.getElementById("gorgias-chat-container")){var o=document.createElement("div");o.id="gorgias-chat-container",document.body.appendChild(o);var r=document.createElement("script");r.setAttribute("defer",!0),r.src="https://client-builds.production.gorgias.chat/{bundleVersion}/static/js/main.js".replace("{bundleVersion}",n.bundleVersion),document.body.appendChild(r)}}else console.error("Failed request GET - https://config.gorgias.chat/applications/3862")},e.onerror=function(_){console.error(_)},e.send()}(window||{});</script><!--Gorgias Chat Widget End-->


                    </script>


                    <div onclick="GorgiasChat.open()();"></div>

                            <script>

                    $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {$("div").trigger('click');}, 1000);});
                      

                    </script>


                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path == "/AgronSales"):

            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Agron</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="https://agron.io/"><img src='/agron/agron_logo.jpg' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#468725;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open();"> Chat with us </span>
                        
                    </center>
                    
<!--Gorgias Chat Widget Start--><script id="gorgias-chat-widget-install">!function(_){_.GORGIAS_CHAT_APP_ID="3862",_.GORGIAS_CHAT_BASE_URL="us-east1-898b.production.gorgias.chat",_.GORGIAS_API_BASE_URL="config.gorgias.chat";var e=new XMLHttpRequest;e.open("GET","https://config.gorgias.chat/applications/3862",!0),e.onload=function(t){if(4===e.readyState)if(200===e.status){var n=JSON.parse(e.responseText);if(!n.application||!n.bundleVersion)throw new Error("Missing fields in the response body - https://config.gorgias.chat/applications/3862");if(_.GORGIAS_CHAT_APP=n.application,_.GORGIAS_CHAT_BUNDLE_VERSION=n.bundleVersion,n&&n.texts&&(_.GORGIAS_CHAT_TEXTS=n.texts),n&&n.sspTexts&&(_.GORGIAS_CHAT_SELF_SERVICE_PORTAL_TEXTS=n.sspTexts),!document.getElementById("gorgias-chat-container")){var o=document.createElement("div");o.id="gorgias-chat-container",document.body.appendChild(o);var r=document.createElement("script");r.setAttribute("defer",!0),r.src="https://client-builds.production.gorgias.chat/{bundleVersion}/static/js/main.js".replace("{bundleVersion}",n.bundleVersion),document.body.appendChild(r)}}else console.error("Failed request GET - https://config.gorgias.chat/applications/3862")},e.onerror=function(_){console.error(_)},e.send()}(window||{});</script><!--Gorgias Chat Widget End-->


                    </script>


                    <div onclick="GorgiasChat.open();"></div>

                            <script>

                    $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {$("div").trigger('click');}, 1000);});
                      

                    </script>


                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path == "/MagnificentCleaning"):

            htmlPage = """

<!DOCTYPE html><meta name="viewport" content="width=device-width, initial-scale=1">
<html dir="ltr" lang="en-US">
    
    <head><title>Magnificent Cleaning</title>
    <style>
        body {

                    text-align: center;
                    width:auto;
                }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" async="" src="https://js.driftt.com/include/1592409000000/4szn78semp8x.js"></script>
<!-- Start of Async Drift Code --> 
<script> 
"use strict"; 

!function() { 
var t = window.driftt = window.drift = window.driftt || []; 
if (!t.init) { 
if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice.")); 
t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
t.factory = function(e) { 
return function() { 
var n = Array.prototype.slice.call(arguments); 
return n.unshift(e), t.push(n), t; 
}; 
}, t.methods.forEach(function(e) { 
t[e] = t.factory(e); 
}), t.load = function(t) { 
var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script"); 
o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js"; 
var i = document.getElementsByTagName("script")[0]; 
i.parentNode.insertBefore(o, i); 
}; 
} 
}(); 
drift.SNIPPET_VERSION = '0.3.1'; 
drift.load('4szn78semp8x'); 
</script> 
<!-- End of Async Drift Code -->
<!-- place this script tag after the Drift embed tag -->
<script>
  (function() {
    /* Add this class to any elements you want to use to open Drift.
     *
     * Examples:
     * - <a class="drift-open-chat">Questions? We're here to help!</a>
     * - <button class="drift-open-chat">Chat now!</button>
     *
     * You can have any additional classes on those elements that you
     * would ilke.
     */
    var DRIFT_CHAT_SELECTOR = '.drift-open-chat'
    /* http://youmightnotneedjquery.com/#ready */
    function ready(fn) {
      if (document.readyState != 'loading') {
        fn();
      } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
      } else {
        document.attachEvent('onreadystatechange', function() {
          if (document.readyState != 'loading')
            fn();
        });
      }
    }
    /* http://youmightnotneedjquery.com/#each */
    function forEachElement(selector, fn) {
      var elements = document.querySelectorAll(selector);
      for (var i = 0; i < elements.length; i++)
        fn(elements[i], i);
    }
    function openSidebar(driftApi, event) {
      event.preventDefault();
      driftApi.sidebar.open();
      return false;
    }
    ready(function() {
      drift.on('ready', function(api) {
        var handleClick = openSidebar.bind(this, api)
        forEachElement(DRIFT_CHAT_SELECTOR, function(el) {
          el.addEventListener('click', handleClick);
        });
      });
    });
  })();
</script>

<!-- simply add 'class="drift-open-chat"' to any link on your site -->
</head>
<body>
<script>
        $(window).on('load', function() {

        setTimeout(
          function() 
          {$("drift-chat-link-chatdesk").trigger('click');}, 800);});
          
        </script>

        <a href="https://themagnificent-cleaning.com/"><img src='/magnificentcleaning/logo-v2.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>

        <span style="color:white;background-color:#1F415E;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" class="drift-open-chat"> Chat with us </span>


        </body>
        </html>


            """

            self.response.write(htmlPage)

        elif(url_path == "/Snow"):

            htmlPage = """<html>
                <head><meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Snow</title>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <style>
                body {

                    text-align: center;
                }

                img {
                    width:auto;
                }
                </style>


                </head>
                <body>
                    <center>
                        <p><a href="https://www.trysnow.com/"><img src='/snow/snow_large.jpg' style='max-width:300px;margin-top:100px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#004c77;padding:20px;margin:20px;width:100px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open()();"> Chat with us </span>
                        
                    </center>
                    <!--Gorgias Chat Widget Start--><div id="gorgias-chat"><script>window.gorgiasChatParameters = {"smooch": {"properties": {"shopify__cart__total_price": "$0.00","shopify__cart__nb_of_items": 0,}}}</script><script src="https://config.gorgias.io/production/P4k1n6jvbv6reOX9/chat/QBGoD2e1Jo2djqlX.js" defer></script></div><!--Gorgias Chat Widget End-->


                    </script>


                    <div onclick="Smooch.open();"></div>

                    <script>

                    $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {$("div").trigger('click');}, 1000);});
                      

                    </script>


                </body>
                </html>
            """

            self.response.write(htmlPage)

        elif(url_path == "/TinaDavies"):

            htmlPage = """<html>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                    <head>
                <title>Tina Davies</title>
                <script src="https://wchat.freshchat.com/js/widget.js"></script>
                </head>
                <body>
                <center>
                        <p><a href="https://tinadavies.com/"><img src='/tinadavies/tina_davies_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        
                    </center>

                <script>
                  window.fcWidget.init({
                    token: "c74bac7c-8731-4718-9626-5aaa93a37ddb",
                    host: "https://wchat.freshchat.com"
                  });

                  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
                 
                  window.fcWidget.user.setProperties({"Source": "Chatdesk"});

                  window.fcWidget.open();
                  window.fcWidget.show();
                </script>

                </body>"""

            self.response.write(htmlPage)

        elif(url_path == "/Morris4X4"):
            htmlPage = """<html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Morris 4X4 Center Support</title>
            </head>
            <body>

                    <center>
                        <p><a href="https://www.morris4x4center.com/"><img src='https://www.morris4x4center.com/skin/frontend/newmorris/default/images/logo-v2.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#626262;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>                    

                    <!-- Start of morris4x4center Zendesk Widget script -->
                    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=a2948beb-d83f-466d-941e-9c341ecfaba7"> </script>
                    <!-- End of morris4x4center Zendesk Widget script -->
                    </body>
                    </html>
            """
            self.response.write(htmlPage)

# Thinx paths

        # elif(url_path == "/Thinx"):
        #     redirectURL = "http://v2.zopim.com/widget/livechat.html?key=dnSWRNLAiknwgeMWZkn0ZDFeLMAV2O0v"

        elif(url_path == "/Thinx"):
            htmlPage = """
            <html>
            <head>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>window.richpanel||(window.richpanel=[]),window.richpanel.q=[],mth=["track","debug","atr"],sk=function(e){return function(){a=Array.prototype.slice.call(arguments),a.unshift(e),window.richpanel.q.push(a)}};for(var i=0;mth.length>i;i++)window.richpanel[mth[i]]=sk(mth[i]);window.richpanel.load=function(e){var i=document,n=i.getElementsByTagName("script")[0],r=i.createElement("script");r.type="text/javascript",r.async=!0,r.src="https://api.richpanel.com/v2/j/"+e+"?version=2.0.0",n.parentNode.insertBefore(r,n)},window.richpanel.ensure_rpuid="",richpanel.load("thinx4022"),richpanel.track("page_view");</script>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Thinx Live Chat</title>
            </head>
            <body>
            <center>
                <p>
                <a href="https://www.shethinx.com/">
                <img src='/thinx/thinx_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
                </p>
                <div>
                <a id="rp-home">
                <span id="chat-button" style="color:white;background-color:black;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer">Chat with us</span>
                </a>
                </div>
            <script>
                $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {$("#chat-button").trigger('click');}, 1000);});
              
            </script>

            </body>
            </html>
            """
            self.response.write(htmlPage)

        elif(url_path == "/ThinxTest"):
            htmlPage = """
            <html>
            <head>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>window.richpanel||(window.richpanel=[]),window.richpanel.q=[],mth=["track","debug","atr"],sk=function(e){return function(){a=Array.prototype.slice.call(arguments),a.unshift(e),window.richpanel.q.push(a)}};for(var i=0;mth.length>i;i++)window.richpanel[mth[i]]=sk(mth[i]);window.richpanel.load=function(e){var i=document,n=i.getElementsByTagName("script")[0],r=i.createElement("script");r.type="text/javascript",r.async=!0,r.src="https://api.richpanel.com/v2/j/"+e+"?version=2.0.0",n.parentNode.insertBefore(r,n)},window.richpanel.ensure_rpuid="",richpanel.load("thinx4022"),richpanel.track("page_view");</script>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Thinx Live Chat</title>
            </head>
            <body>
            <center>
                <p>
                <a href="https://www.shethinx.com/">
                <img src='/thinx/thinx_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
                </p>
                <div>
                <a id="rp-home">
                <span id="chat-button" style="color:white;background-color:black;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer">Chat with us</span>
                </a>
                </div>
            <script>
                $(window).on('load', function() {

                    setTimeout(
                      function() 
                      {$("#chat-button").trigger('click');}, 1000);});
              
            </script>

            </body>
            </html>
            """
            self.response.write(htmlPage)

        elif(url_path == '/ThinxReturns'):
            redirectURL = "https://thinx.zendesk.com/hc/en-us/articles/360037380692-I-would-like-to-return-my-item?auth_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjo1NTY0NjYsInVzZXJfaWQiOm51bGwsInRpY2tldF9pZCI6bnVsbCwiZGVmbGVjdGlvbl9pZCI6MzYwMzY4NjQ0MTcyLCJhcnRpY2xlcyI6WzM2MDAzNzY1Njc1MSwzNjAwNDE1MzM0NTEsMzYwMDM3MzgwNjkyXSwidG9rZW4iOm51bGwsImV4cCI6MTU5MTgwNDU4NX0.pk2VvL5ZkFFmzg3Rk0VYWj9cILUCiV6pi1eRsY-Rmpc"

        if(redirectType == "browser"):
            if(redirectURL != ""):
                htmlPage = "<html><head><script>window.location = '%s';</script></head><body></body></html>" % redirectURL
            else:
                htmlPage = "<html></html>"

            self.response.write(htmlPage)

        elif(url_path == "/TryGrain"):
            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Freshly Support</title>

                        <!-- Start of chatdesk Zendesk Widget script -->
                    <script type="text/javascript" id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key={}"> </script>
                    <script src="https://v2.zopim.com/?6KVETGhtgvVs2QIysISvvNHpyqylt4mE"></script>
                <!-- End of chatdesk Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.trygrain.com"><img src='/grain/grain_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#268C59;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>
                    
                    <script type="text/javascript">
  
                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                // Hide the Drop-down list option in the Pre-Chat Form to select the Department 
                                //                                   
                                $zopim.livechat.departments.filter('');

                                // Automatically set the Department 
                                //                                    
                                $zopim.livechat.departments.setVisitorDepartment('Fossil eCom');

                                $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>
                </body>
                </html>
            """
            # color =  #009ee1

            htmlTemp = """
            <html><body>
            <center>
                        <p><a href="http://www.trygrain.com"><img src='/grain/grain_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#009ee1;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>
            </body></html>
            """

            self.response.write(htmlTemp)
            # redirectURL = "https://trygrain.com/support/"
        elif(url_path == "/Moo"):
            # moo code = 2070511
            htmlPage = """
            <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Moo</title>
    </head>
    <body>
        <center>
        <a href="http://moo.com/">
            <img style="margin-top:70px;margin-bottom:40px;width:300px" src="/moo/MOO_Logo_Hero-Green_RGB-01.png">
        </a>
        <p>
        <span style="color:white;background-color:#008856;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="LC_API.open_chat_window();return false;">Chat with us</span>
        </p>
        </center>
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script>
        window.__lc = window.__lc || {};
        window.__lc.license = 2070511;
        ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
        </script>
        <noscript><a href="https://www.livechatinc.com/chat-with/2070511/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
        <!-- End of LiveChat code -->
        
    </body>
</html>
            """
            self.response.write(htmlPage)

        elif(url_path == "/MooEmail"):
            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Moo Support</title>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.moo.com"><img src='/moo/MOO_Logo_Hero-Green_RGB-01.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <a id="email_link" href="mailto:support@moo.com?subject=Sales%20request" style="text-decoration:none;"><span style="color:white;background-color:#008856;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer"> Email us </span></a>                        
                    </center>
                </body>
            </html>"""

            self.response.write(htmlPage)

        # elif(url_path == "/SSENSE"):
        #     redirectURL = "http://v2.zopim.com/widget/livechat.html?key=4EUlI6cuYHKKYy7MRbJs2FL8EdPFQrrS"

        elif(url_path == "/SSENSE"):
            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>SSENSE</title>
                    <!--Start of Zendesk Chat Script-->
                    <script type="text/javascript">
                        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
                    d.createElement(s),e=d.getElementsByTagName(s)[0];
                    z.set=function(o){z.set.
                    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
                    $.src="https://v2.zopim.com/?4EUlI6cuYHKKYy7MRbJs2FL8EdPFQrrS";z.t=+new Date;$.
                    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
                    </script>
                    <!--End of Zendesk Chat Script-->
          
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
                    </script>
                </head>
                <body>
                    <center>
                        <p>
                            <a href="https://www.ssense.com/"><img src='./ssense/ssense_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'>
                            </a>
                        </p>

                        <span style="color:white;background-color:#000000;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                                
                    </center>
                    <script type="text/javascript">

                        $(document).ready(function(){{
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          };
                        });
                
                    </script>
                        

                </body>
            </html>
            """
            self.response.write(htmlPage)


        elif(url_path == "/Endy"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=Srxefj5fKZoc08EqXNSggwUNj4IJz8L4"

        elif(url_path == "/EndyFR"):
            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Endy</title>

                            <!-- Start of endy Zendesk Widget script -->
                            <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=e87c643e-b914-44c4-8fd6-5139ca8e2344"></script>
                            <!-- End of endy Zendesk Widget script -->

                            
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p>
                        <a href="http://qc.endy.com"><img src='./endy/endy_logo_2.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'>
                        </a>
                        </p>
                            <span style="color:white;background-color:#c4045c;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                            
                    </center>
                    <script type="text/javascript">

                        $(document).ready(function(){
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
                
                    </script>

                    <script type="text/javascript">
                        zE('webWidget', 'setLocale', 'fr');
                      window.zESettings = {
                        webWidget: {
                          chat: {
                            prechatForm: {
                              greeting: {
                                '*': 'Hi there, thanks for visiting Endy! Please provide your name and email address, then let us know what can we help you with :)',
                                fr: "S'il vous plaît remplir le formulaire ci-dessous pour discuter avec nous"
                              }
                            }
                          }
                        }
                      };
                    </script>
                </body>
            </html>
            """

            self.response.write(htmlPage)

        elif(url_path == "/Feelgoodz"):
            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Feelgoodz</title>

                    <!-- Start of Feelgoodz Zendesk Widget script -->
                    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=91e92669-4ddb-4b02-92d4-ef79655b60de"></script>
                    <!-- End of Feelgoodz Zendesk Widget script -->


                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.feelgoodz.com"><img src='./feelgoodz/feelgoodz.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#f48333;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>
                    <script type="text/javascript">
  
                        $(document).ready(function(){

                            window.zESettings = {
                              webWidget: {
                                helpCenter: {
                                  suppress: true
                                }
                              }
                            };


                        
                          zE(function(){
                            $zopim(function(){

                                $zopim.livechat.setOnConnected(function() {

                                    $zopim.livechat.button.setPositionMobile('bl');

                                    $zopim.livechat.window.show();
                                });                                    
                            });
                          });
                        });
 
                
                        </script>

                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/TinaDaviesAccount"):

            redirectURL = "https://tinadavies.com/account"

        # elif(url_path == "/GrubhubApp"):

        #     if "iPhone" in self.request.headers["User-Agent"] or "iPad" in self.request.headers["User-Agent"]:

        #         redirectURL = "https://apps.apple.com/us/app/grubhub-for-drivers/id1452071632"

        #     else:

        #         redirectURL = "https://play.google.com/store/apps/details?id=com.grubhub.driver&hl=en&gl=US"

        elif(url_path == "/GrubhubEmail"):
            htmlPage = """
            <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Grubhub</title>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.grubhub.com"><img src='/grubhub/grubhub_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <a id="email_link" href="mailto:driverpayment@grubhub.com?subject=Payment Question" style="text-decoration:none;"><span style="color:white;background-color:#f43444;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer"> Email us </span></a>                        
                    </center>
                </body>
            </html>"""

            self.response.write(htmlPage)

        elif(url_path == "/KOHO"):
            htmlPage ="""<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>KOHO Support</title>

    <script>
    function getMobileOperatingSystem() {
      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

          // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }
    </script>

    <script>
    function DetectAndServe(){
        let os = getMobileOperatingSystem();
        if (os == "Android") {
            window.location.href = "https://play.google.com/store/apps/details?id=ca.koho"; 
        } else if (os == "iOS") {
            window.location.href = "https://apps.apple.com/ca/app/koho-personal-finance/id1091010942";
        } else {
            window.location.href = "https://www.koho.ca/";
        }
    }
    </script>
    <script>
    function deepLinkOrForward() {
        window.location = "koho://app/link?dp=%2Fintercom%2Fmessenger&dp_source=intercom";
        setTimeout("DetectAndServe();", 1000);
    }
    </script>
</head>
<body onload="deepLinkOrForward()"></body>
</html>"""

            self.response.write(htmlPage)

        elif(url_path == "/GrubhubDelivery"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/categories/115001254806-Completing-Deliveries"

        elif(url_path == "/GrubhubTax"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/sections/360004900531-Taxes"

        elif(url_path == "/GrubhubGetStarted"):
           redirectURL = "https://driver.grubhub.com/faq/"

        elif(url_path == "/GrubhubPay"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/categories/115001254786-Getting-Paid" 

        elif(url_path == "/GrubhubInstant"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/sections/360007031752-Instant-Cash-Out"

        elif(url_path == "/GrubhubDriverQuestions"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/requests/new?ticket_form_id=360000782592"

        elif(url_path == "/GrubhubDinerNoAnswer"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/articles/360029447452-The-diner-is-not-answering-my-call-What-can-I-do-"

        elif(url_path == "/GrubhubDriverDelay"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/articles/360029442532-I-m-going-to-be-late-delivering-an-order-What-can-I-do-"

        elif(url_path == "/GrubhubRestaurantDelay"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/articles/360029762711-I-ve-accepted-an-order-that-is-already-late-What-can-I-do-"

        elif(url_path == "/GrubhubRestaurantClosed"):
           redirectURL = "https://driver-support.grubhub.com/hc/en-us/articles/360036087651-What-do-I-do-if-the-restaurant-is-closed-"

        elif(url_path == "/SwitchHealth"):
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "zzk70niy"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('showNewMessage');</script>
                <center>
                <a href="http://https://www.switchhealth.ca/">
                    <img style="margin-top:70px;margin-bottom:40px;width:300px" src="/switch_health/switch_logo.jpg">
                </a>
                <p>
                <span style="color:white;background-color:#2596be;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="Intercom('showNewMessage');return false;">Chat with us</span>
                </p>
                </center>
                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path[:14] == "/SwitchHealth/"):

            from hashids import Hashids
            hashids = Hashids()
            cur_hashid = url_path[14:]            
            customer_phone_number = hashids.decode(cur_hashid)[0]
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "zzk70niy",
                phone: "%s"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('showNewMessage');</script>
                <center>
                <a href="https://www.switchhealth.ca/">
                    <img style="margin-top:70px;margin-bottom:40px;width:300px" src="/switch_health/switch_logo.jpg">
                </a>
                <p>
                <span style="color:white;background-color:#2596be;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="Intercom('showNewMessage');return false;">Chat with us</span>
                </p>
                </center>
                </body>
                </html>""" % (customer_phone_number)
            
            self.response.write(htmlPage)

        elif(url_path == "/SPData"):
            htmlPage = """
            <!doctype html>
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>S&P Data</title>
            </head>
            <body>


            <center>
                <p>
                    <a href="https://www.spdatallc.com/"><img src='/snpdata/S_and_P_Logo.jpg' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a>
                </p>

                        
                    <span style="color:white;background-color:#358ba6;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="GorgiasChat.open();"> Chat with us </span>
                        
                                
                </center>

                    <div onclick="GorgiasChat.open();"></div>

                    <script>

                    $(window).on('load', function(_) {

                    setTimeout(
                      function() 
                      {$("div").trigger('click');}, 1000);});
                      

                    </script>
                    
             <!--Gorgias Chat Widget Start--><script id="gorgias-chat-widget-install">!function(_){_.GORGIAS_CHAT_APP_ID="6745",_.GORGIAS_CHAT_BASE_URL="us-east1-898b.production.gorgias.chat",_.GORGIAS_API_BASE_URL="config.gorgias.chat";var e=new XMLHttpRequest;e.open("GET","https://config.gorgias.chat/applications/6745",!0),e.onload=function(t){if(4===e.readyState)if(200===e.status){var n=JSON.parse(e.responseText);if(!n.application||!n.bundleVersion)throw new Error("Missing fields in the response body - https://config.gorgias.chat/applications/6745");if(_.GORGIAS_CHAT_APP=n.application,_.GORGIAS_CHAT_BUNDLE_VERSION=n.bundleVersion,n&&n.texts&&(_.GORGIAS_CHAT_TEXTS=n.texts),n&&n.sspTexts&&(_.GORGIAS_CHAT_SELF_SERVICE_PORTAL_TEXTS=n.sspTexts),!document.getElementById("gorgias-chat-container")){var o=document.createElement("div");o.id="gorgias-chat-container",document.body.appendChild(o);var r=document.createElement("script");r.setAttribute("defer",!0),r.src="https://client-builds.production.gorgias.chat/{bundleVersion}/static/js/main.js".replace("{bundleVersion}",n.bundleVersion),document.body.appendChild(r)}}else console.error("Failed request GET - https://config.gorgias.chat/applications/6745")},e.onerror=function(_){console.error(_)},e.send()}(window||{});</script><!--Gorgias Chat Widget End-->
            </body>
            </html>
            """

            self.response.write(htmlPage)

        elif(url_path == "/YourSuper"):
            redirectURL = "http://v2.zopim.com/widget/livechat.html?key=SQi1s3jXAifVlnS4YJ7fAkV5Rlw6C8Ic"

        elif(url_path == "/Blacklane"):
            
            htmlPage = """<html><meta name="viewport" content="width=device-width, initial-scale=1">
                <body>
                
                <script>
                window.intercomSettings = {
                app_id: "bc61mu8p"
                };
                </script>
                <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/wn222jss';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
                <script>Intercom('show');</script>
                <center>
                <a href="https://www.blacklane.com/">
                    <img style="margin-top:70px;margin-bottom:40px;width:300px" src="/blacklane/blacklane_logo.jpg">
                </a>
                <p>
                <span style="color:white;background-color:#000000;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="Intercom('showNewMessage');return false;">Chat with us</span>
                </p>
                </center>
                </body>
                </html>
                </body>
                </html>"""
            
            self.response.write(htmlPage)

        elif(url_path == "/Archways"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>ISTS</title>

                    <!-- Start of ISTS Zendesk Widget script -->

                    <script id="ze-snippet" src=https://static.zdassets.com/ekr/snippet.js?key=44687aa5-37de-4403-9b5e-2d91b1498ee0> </script>

                    <!-- End of ISTS Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.applyists.com"><img src='/ISTS/ists-logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#75994f;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>

                    <script type="text/javascript">

                    $(document).ready(function(){
                        window.zESettings = {
                            webWidget: {
                              chat: {
                                departments: {
                                  select: 'Archways to Opportunity'
                                }
                              }
                            }
                          };
                      zE(function(){
                        $zopim(function(){

                            $zopim.livechat.setOnConnected(function() {

                                $zopim.livechat.window.show();
                            });                                    
                        });
                      });
                    });


                </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path == "/HEB"):

            htmlPage = """

                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>ISTS</title>

                    <!-- Start of ISTS Zendesk Widget script -->

                    <script id="ze-snippet" src=https://static.zdassets.com/ekr/snippet.js?key=44687aa5-37de-4403-9b5e-2d91b1498ee0> </script>

                    <!-- End of ISTS Zendesk Widget script -->

                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                </head>
                <body>
                    <center>
                        <p><a href="http://www.applyists.com"><img src='/ISTS/ists-logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                        <span style="color:white;background-color:#75994f;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                        
                    </center>

                    <script type="text/javascript">

                    $(document).ready(function(){
                        window.zESettings = {
                            webWidget: {
                              chat: {
                                departments: {
                                  select: 'Scholarship, Grant or Loan'
                                }
                              }
                            }
                          };
                      zE(function(){
                        $zopim(function(){

                            $zopim.livechat.setOnConnected(function() {

                                $zopim.livechat.window.show();
                            });                                    
                        });
                      });
                    });


                </script>
                </body>
                </html>



            """

            self.response.write(htmlPage)

        elif(url_path=="/BigGirlsDontCry"):
            htmlPage = """
                <html>
                <head>
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                        <title>Big Girls Don't Cry</title>
                </head>
                <body>
                    <center>
                        <p><a href="https://www.brastogo.com.au/"><img src='/curvy/bgdc_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'>
                        </a>
                        </p>
                        
                        <span style="color:white;background-color:#ff0279;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer;" onclick="tidioChatApi.open()"> Chat With Us </span> 
                      
                    </center>
                <script async="" src="//code.tidio.co/qvsbbiukupq9srojkz0ckj6qufhysvrb.js"></script>
                <script type="text/javascript">
                    (function() {
                      function onTidioChatApiReady() {
                        window.tidioChatApi.open();
                      }
                      if (window.tidioChatApi) {
                        window.tidioChatApi.on("ready", onTidioChatApiReady);
                      } else {
                        document.addEventListener("tidioChat-ready", onTidioChatApiReady);
                      }
                    })();
                </script>

                </body>
                </html>
                """
            self.response.write(htmlPage)


        elif(url_path == "/Noom"):
            htmlPage = """
            <html>
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Noom</title>

                <!-- Start of Noom Zendesk Widget script -->

                <script type='text/javascript' id='noom-support-zendesk-js-js'>
                (function ($) {

                function showChatSupportButton() {
                zE('webWidget', 'hide');
                    $("#chat-support").animate({bottom:"+=100px"}, "slow"); 
                }

                function hideChatSupportButton() {
                    $("#chat-support").animate({bottom:"-=100px"}, "slow"); 
                }

                var waitForZopim = setInterval(function () {
                    if (window.$zopim === undefined || window.$zopim.livechat === undefined) {
                        return;
                    }
                    $zopim(function() {


                        /** Zendesk: Hiding Zendesk Chat support widget **/
                        zE('webWidget', 'hide');

                        /** Noom: Showing Noom Chat support **/
                        showChatSupportButton();

                        /** Mixpanel: Tracking support button displayed **/
                        mixpanel.track("HelpPageZendeskWidgetDisplayed");

                        /** Noom: Showing Zendesk Chat on support button click **/
                        $( "#chat-support" ).on( "click", function (e) {

                            /** Mixpanel: Tracking support button click **/
                            mixpanel.track("HelpPageZendeskWidgetClicked");

                            /** Zendesk: Showing Zendesk Chat support widget **/
                            zE('webWidget', 'show');
                            
                            /** Zendesk: Opening Zendesk Chat support **/
                            zE('webWidget', 'open');
                        });

                        /** Zendesk: Setting up call-backs when closing/opening Zendesk chat support **/
                        zE('webWidget:on', 'close', showChatSupportButton);
                        zE('webWidget:on', 'open', hideChatSupportButton);

                    });
                    clearInterval(waitForZopim);
                }, 100);

                })
                    

                </script>
                <!-- End of Noom Zendesk Widget script -->

            </head>
            <body>
                <center>
                    <p><a href="http://www.noom.com/"><img src='/noom/noom_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                    <span style="color:white;background-color:#F18F3C;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="$zopim.livechat.window.show();"> Chat with us </span>
                    
                </center>

                <!-- Start of noom Zendesk Widget script -->
            <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=999f88cf-e4df-4488-841f-6e61f3cb60c4"> </script>
            <script type="text/javascript">zE('webWidget', 'hide');</script>
            <script type="text/javascript">
                window.zESettings = { 
                    webWidget: { 
                        chat: { 
                            departments: { 
                                enabled: ['Billing Support', 'Coaching Support', 
                                'Registration Support', 'Technical Support', 'Other', 'Add On Support'] 
                            } 
                        } 
                    } 
                };

                zE(function(){
                    $zopim(function(){

                        $zopim.livechat.setOnConnected(function() {

                            $zopim.livechat.button.setPositionMobile('br');
                            $zopim.livechat.window.show();
                        });  
                    });
                });
            </script>

                
            </body>
            </html>
            """
            self.response.write(htmlPage)

        elif(url_path == "/Coil"):
            htmlPage = """
            <html>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <head>
            <title>Coil</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

            </head>
            <body>

                <center>
                    <p><a href="http://www.coil.com"><img src='/coil/coil_logo.png' style='max-width:300px;margin-top:70px;margin-bottom:40px'></a></p>
                    <span style="color:white;background-color:#000;padding:20px;margin:20px;width:150px;font-family:'Arial';font-size:20px;border-radius: 10px;cursor:pointer" onclick="FrontChat('show');"> Chat with us </span>
                    
                </center>


            <script src="https://chat-assets.frontapp.com/v1/chat.bundle.js"></script>
            <script>
              window.FrontChat('init', {chatId: '5d55a6e3a07b100840928c74d10c5d50', useDefaultLauncher: true});        
            </script>
            <script>
                FrontChat('show');
                    $(document).ready(function(){
                    FrontChat('show');

                });

        
            </script>
            </body>
            </html>
            """
            self.response.write(htmlPage)

        ## END OF ORG CODE

        elif(url_path == "/"):
            redirectURL = "https://www.chatdesk.com/shift"
        
        elif(url_path == "/AFTest"):
            htmlTemp = """
            <html><body>
            <p> this is a test </p>
            </body></html>
            """
            self.response.write(htmlTemp)

### END OF ORG CONDITIONAL CODE

        # obtain ip address
        ip_address = self.request.remote_addr

        link_click = ShortLink()
#        link_click.ip_address = str(ip_address)
        link_click.url_path = url_path
        link_click.put()

        if(redirectType == "server" and followRedirect and redirectURL != ""):
           self.redirect(redirectURL)



def utc_to_time(naive, timezone="Europe/Istanbul"):

    import pytz
    
    return naive.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(timezone))

"""
class MintedURLCounter(webapp2.RequestHandler):
  def get(self):

    from google.appengine.ext import ndb
    import datetime

#    CurTZ = "America/Los_Angeles"

    CurTZ = "America/New_York"


    Link_path = "/Minted/"

    link_clicks = ndb.gql("SELECT * FROM ShortLink WHERE url_path > :linkpath", linkpath=Link_path)

    self.response.write("<html><body>")
    for cur_row in link_clicks:
        cur_path = cur_row.url_path 
        if(Link_path in cur_path):

            PST_date = utc_to_time(cur_row.click_date, CurTZ)            
            date_string = PST_date.strftime('%Y-%m-%d %H:%M:%S')
            cur_line = date_string + "," + cur_path + "<br>"
            self.response.write(cur_line)
    self.response.write("</body></html>")

"""

class KustomerTest(webapp2.RequestHandler):
  def post(self):
      
      logging.info(self.request.params.items())

      

app = webapp2.WSGIApplication([
#  ('/minted_url_counter', MintedURLCounter),
  ('/kustomer_test', KustomerTest),
  ('/.*', urlRedirect)
], debug=True)
